﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiReturViewModel
    {
        public string NoRetur { get; set; }
        public string NoBilling { get; set; }
        public DateTime Tanggal { get; set; }
        public string NoReg { get; set; }
        public string Alasan { get; set; }
        public int SubTotal { get; set; }
        public List<FarmasiReturDetailViewModel> Detail { get; set; }
    }

    public class FarmasiReturDetailViewModel 
    {
        public int Id { get; set; }
        public string NoBilling { get; set; }
        public double QtryRetur { get; set; }
    }
}