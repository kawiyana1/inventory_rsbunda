﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PermintaanPembelianViewModel
    {
        public DateTime Tanggal { get; set; }
        public int Id { get; set; }
        public string NoBukti { get; set; }
        public int Gudang { get; set; }
        public DateTime? TanggalDibutuhkan { get; set; }
        public int KelompokPR { get; set; }
        public string Keterangan { get; set; }
        public decimal Total { get; set; }
        public int SupplierID { get; set; }
        public List<PermintaanPembelianDetailViewModel> Detail { get; set; }
    }

    public class PermintaanPembelianDetailViewModel
    {
        public int Id { get; set; }
        public decimal Harga { get; set; }
        public double Qty { get; set; }
    }
}