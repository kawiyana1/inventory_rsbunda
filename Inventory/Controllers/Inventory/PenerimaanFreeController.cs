﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PenerimaanFreeController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            var model = new PenerimaanFreeViewModel();
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            using (var s = new SIMEntities())
            {
                model.PPNNilai = 11;
                var setup = s.SetupAwal.FirstOrDefault(x => x.SetupName == "PPNPersen");
                if (setup != null)
                {
                    model.PPNNilai = setup.Nilai.ToDecimal();
                }
            }
            ViewBag.PPNNilai = model.PPNNilai;
            return View(model);
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPenerimaanFree.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_DO.Contains(x.Value) ||
                                y.No_Bonus.Contains(x.Value) ||
                                y.Supplier_Nama_Supplier.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Bonus >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Bonus <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Bonus == null ? "" : m.Tgl_Bonus.Value.ToString("dd/MM/yyyy"),
                        No = m.No_Bonus,
                        NoDO = m.No_DO,
                        Nilai = m.Total_Nilai,
                        Batal = m.Batal,
                        Supplier = m.Supplier_Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PenerimaanFreeViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Supplier == 0) throw new Exception("Supplier Tidak Boleh Kosong");
                            if (model.Detail == null) model.Detail = new List<PenerimaanFreeDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            foreach (var x in model.Detail) 
                            {
                                if (x.Qty <= 0) throw new Exception("Qty tidak boleh kurang dari sama dengan 0");
                            }
                            id = s.INV_InsertPenerimaanFree(
                                model.Tanggal,
                                model.NoDO,
                                model.PPN,
                                model.PPNNilai,
                                model.Supplier,
                                model.Lokasi,
                                model.Total,
                                model.MataUang
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertPenerimaanFree tidak mendapatkan nobukti");

                            foreach (var x in model.Detail)
                            {
                                var jum = x.Qty * x.Harga;
                                var diskon = jum / 100 * (decimal)x.Disc;
                                s.INV_InsertPenerimaanFreeDetail(
                                    id,
                                    x.Id,
                                    x.Qty, 
                                    x.Harga,
                                    x.Disc,
                                    (x.Harga * (decimal)x.Qty) / 100 * (decimal)x.Disc,
                                    model.Lokasi
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.INV_InsertPenerimaanFreeBatal(model.No);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListPenerimaanFreeHeader.FirstOrDefault(x => x.No_Bonus == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListPenerimaanFreeDetail.Where(x => x.No_Bonus == m.No_Bonus).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No = m.No_Bonus,
                            Tanggal = m.Tgl_Bonus == null ? "" : m.Tgl_Bonus.Value.ToString("yyyy-MM-dd"),
                            NoDO = m.No_DO,
                            Supplier = m.Supplier_ID,
                            Lokasi = m.Lokasi_ID,
                            MataUang = m.Currency_ID,
                            PPN = m.PPN,
                            PPNNilai = m.NilaiPPN,
                            Total = m.Total_Nilai
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Kode_Satuan,
                            Qty = x.Qty,
                            Harga = x.Harga,
                            Disc = x.Diskon_Persen
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_LookupBarang.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kategori.Contains(x.Value));
                        else if (x.Key == "Lokasi")
                        {
                            var _v = int.Parse(x.Value);
                            proses = proses.Where(y => y.Lokasi_ID == _v);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                        Stok = x.Qty_Stok,
                        Kategori = x.Kategori,
                        MinStok = x.Min_Stok,
                        MaxStok = x.Max_Stok,
                        QtyStok = x.Qty_Stok,
                        Harga = x.Harga_Beli
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}