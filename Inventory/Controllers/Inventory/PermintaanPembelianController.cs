﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PermintaanPembelianController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPermintaanPembelian.Where(x => x.Gudang_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Permintaan.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoPermintaan = m.No_Permintaan,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Status = m.Status,
                        Status_Batal = m.Status_Batal,
                        NamaSupplier = m.Nama_Supplier,
                        Total = m.Total,
                        TanggalDibutuhkan = m.Tgl_Dibutuhkan == null ? "" : m.Tgl_Dibutuhkan.Value.ToString("dd/MM/yyyy")
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PermintaanPembelianViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        s.Database.CommandTimeout = 180;
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        var nopenerimaan = "";
                        
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<PermintaanPembelianDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            foreach (var x in model.Detail)
                            {
                                //if (x.Qty <= 0) throw new Exception("Qty tidak boleh kurang dari sama dengan 0");
                            }
                            id = s.INV_InsertPermintaan(
                                model.Tanggal,
                                model.Gudang,
                                model.TanggalDibutuhkan,
                                model.KelompokPR,
                                model.Keterangan,
                                model.Total,
                                model.SupplierID,
                                userid).FirstOrDefault() ?? 0;
                            nopenerimaan = id.ToString();
                            var m = s.INV_ListPermintaanPembelian.FirstOrDefault(x => x.Permintaan_ID == id);
                            var id_order = s.INV_inserOrder(
                                model.Tanggal,
                                model.Keterangan,
                                model.SupplierID,
                                model.Total,
                                m.No_Permintaan,
                                model.KelompokPR
                            ).FirstOrDefault();

                            if (id == 0) throw new Exception("INV_InsertPermintaan tidak mendapatkan id");
                            
                            foreach (var x in model.Detail)
                            {
                                s.INV_InsertPermintaanDetail(
                                    (int)id,
                                    x.Id,
                                    x.Qty,
                                    x.Harga,
                                    model.Gudang
                                );
                                s.INV_inserOrderDetail(
                                    x.Qty,
                                    x.Harga,
                                    m.No_Permintaan,
                                    (int)id_order.OrderID,
                                    x.Id
                                );
                                if (x.Qty == 0) throw new Exception("QTY Tidak Boleh Kosong");
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<PermintaanPembelianDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            s.INV_UpdatePermintaan(
                                model.Id,
                                model.Tanggal,
                                model.Gudang,
                                model.TanggalDibutuhkan,
                                model.KelompokPR,
                                model.Keterangan,
                                model.Total,
                                model.SupplierID,
                                userid
                            );

                            s.INV_UpdateOrder(
                                model.Id,
                                model.Tanggal,
                                model.Gudang,
                                model.TanggalDibutuhkan,
                                model.KelompokPR,
                                model.Keterangan,
                                model.Total,
                                model.SupplierID,
                                userid
                            );

                            var m = s.INV_ListPermintaanPembelian.FirstOrDefault(x => x.Permintaan_ID == model.Id);
                            var order = s.BL_trOrder.FirstOrDefault(x => x.No_Permintaan == m.No_Permintaan);
                            var d = s.INV_ListPermintaanPembelianDetail.Where(x => x.Permintaan_ID == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
                                //delete
                                if (_d == null) { 
                                    s.INV_DeletePermintaanDetail(model.Id, x.Barang_ID);
                                    s.INV_DeleteOrderDetail(model.Id, x.Barang_ID);
                                }
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.Barang_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.INV_InsertPermintaanDetail(
                                        model.Id,
                                        x.Id,
                                        x.Qty,
                                        x.Harga,
                                        model.Gudang
                                    );
                                    s.INV_inserOrderDetail(
                                        x.Qty,
                                        x.Harga,
                                        m.No_Permintaan,
                                        (int)order.Order_ID,
                                        x.Id
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.INV_UpdatePermintaanDetail(model.Id, x.Id, x.Qty);
                                    s.INV_UpdateOrderDetail(model.Id, x.Id, x.Qty);
                                }

                            }

                            //var d_detailorder = s.INV_ListPermintaanPembelianDetail.Where(x => x.Permintaan_ID == model.Id).ToList();
                            //foreach (var x in d_detailorder)
                            //{
                            //    var _d_detailorder = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
                            //    //delete
                            //    if (_d_detailorder == null) s.INV_DeleteOrderDetail(model.Id, x.Barang_ID);

                            //}
                            //foreach (var x in model.Detail)
                            //{
                            //    var _d_detailorder = d.FirstOrDefault(y => y.Barang_ID == x.Id);
                            //    if (_d_detailorder == null)
                            //    {
                            //        // new
                            //        s.INV_inserOrderDetail(
                            //        x.Qty,
                            //        x.Harga,
                            //        m.No_Permintaan,
                            //        (int)id_order.OrderID,
                            //        x.Id
                            //    );
                            //    }
                            //    else
                            //    {
                            //        // edit
                            //        s.INV_UpdatePermintaanDetail(model.Id, x.Id, x.Qty);
                            //    }

                            //}

                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Amprahan-PR-CREATE; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                  
                    var m = s.INV_ListPermintaanPembelian.FirstOrDefault(x => x.No_Permintaan == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListPermintaanPembelianDetail.Where(x => x.Permintaan_ID == m.Permintaan_ID);
                    var sectionlogin = Request.Cookies["Inventory_Section_Id"].Value;
                    var res = d.OrderBy(sectionlogin == "SEC031" ? "Kode_Barang ASC" : "Nama_Barang ASC").ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Permintaan_ID,
                            NoPermintaan = m.No_Permintaan,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            Gudang = m.Gudang_ID,
                            TanggalDibutuhkan = m.Tgl_Dibutuhkan == null ? "" : m.Tgl_Dibutuhkan.Value.ToString("yyyy-MM-dd"),
                            KelompokPR = m.JenisPengadaanID,
                            Keterangan = m.Keterangan,
                            SupplierID = m.Supplier_ID,
                            NamaSupplier = m.Nama_Supplier
                        },
                        Detail = res.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Konversi = x.Konversi,
                            Kategori = x.Nama_Kategori,
                            Satuan = x.Satuan,
                            MinStok = x.Min_Stok,
                            MaxStok = x.Max_Stok,
                            QtyStok = x.Qty_Stok,
                            Qty = x.Qty,
                            Harga = x.Harga_Beli
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_LookupBarang_Penerimaan.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kategori.Contains(x.Value));
                        else if (x.Key == "Lokasi")
                        {
                            var _v = int.Parse(x.Value);
                            proses = proses.Where(y => y.Lokasi_ID == _v);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                        Stok = x.Qty_Stok,
                        Kategori = x.Kategori,
                        MinStok = x.Min_Stok,
                        MaxStok = x.Max_Stok,
                        QtyStok = x.Qty_Stok,
                        Harga = x.Harga_Beli
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}