//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Inventory_BL_trPermintaan
    {
        public Nullable<short> Supplier_ID { get; set; }
        public short Currency_ID { get; set; }
        public decimal Permintaan_ID { get; set; }
        public System.DateTime Tgl_Permintaan { get; set; }
        public string No_Permintaan { get; set; }
        public Nullable<byte> Departemen_ID { get; set; }
        public Nullable<System.DateTime> Tgl_Dibutuhkan { get; set; }
        public string Keterangan { get; set; }
        public Nullable<short> User_ID { get; set; }
        public System.DateTime Tgl_Update { get; set; }
        public bool Status_Batal { get; set; }
        public Nullable<int> Gudang_ID { get; set; }
        public string Status { get; set; }
        public string Kode_Proyek { get; set; }
        public Nullable<bool> Pembelian_Asset { get; set; }
        public Nullable<int> JenisPengadaanID { get; set; }
        public string Username { get; set; }
        public Nullable<System.DateTime> Tgl_Order { get; set; }
        public string Nama_Supplier { get; set; }
        public string Nama_Lokasi { get; set; }
        public string JenisPengadaan { get; set; }
    }
}
