﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiReturController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListRetur.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoRetur.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoRetur = m.NoRetur,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{m.Jam.ToString("HH")}:{m.Jam.ToString("mm")}",
                        NoReg = m.Transaksi_NoReg,
                        NRM = m.NRM,
                        NamaPasien = m.NamaPasien,
                        JenisKerjasama = m.Transaksi_JenisKerjasama,
                        Perusahaan = m.Nama_Customer
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiReturViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            //var rspsave = s.FAR_GetReturHeader.FirstOrDefault(x => x.NoReg == model.NoReg);
                            var transaksibill = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == model.NoReg);
                            if (transaksibill.StatusBayar == "Sudah Bayar") throw new Exception("Pasiem sudah melalukan pembayaran");
                            if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                            var section = Request.Cookies["Inventory_Section_Id"].Value;
                            if (model.Detail == null) model.Detail = new List<FarmasiReturDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            //if (model.SubTotal == 0) throw new Exception("Total Qty Retur Boleh Kosong");
                            id = s.FAR_InsertReturHeader(
                                model.NoBilling,
                                model.Tanggal,
                                model.Tanggal,
                                section,
                                model.Alasan,
                                model.NoReg,
                                userid
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("FAR_InsertReturHeader tidak mendapatkan nobukti");
                            
                            foreach (var x in model.Detail)
                            {
                                //if(x.QtryRetur == 0) throw new Exception("Qty Retur Tidak Boleh 0");
                                s.FAR_InsertReturDetail(
                                    id,
                                    x.Id,
                                    x.QtryRetur,
                                    x.NoBilling
                                );
                            }
                            s.HitungEstimasiBiaya(model.NoReg);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiRetur-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_GetReturHeader.FirstOrDefault(x => x.NoRetur == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.FAR_GetReturDetail.Where(x => x.NoRetur == m.NoRetur).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoRetur = m.NoRetur,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            Jam = $"{m.Jam:HH}:{m.Jam:mm}",
                            Section = m.SectionID,
                            NoReg = m.NoReg,
                            NRM = m.NRM,
                            Nama = m.NamaPasien,
                            NoBilling = m.NoBukti,
                            TanggalBilling = m.TglBilling == null ? "" : m.TglBilling.Value.ToString("yyyy-MM-dd"),
                            JamBilling = m.JamBilling == null ? "" : $"{m.JamBilling.Value:HH}:{m.JamBilling.Value:mm}",
                            Alasan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            QtyPesan = x.Qty_Pesan,
                            NoBill = x.NoBill,
                            QtyTerpakai = x.Qty_Terpakai,
                            QtryRetur = x.Qty_Retur
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B A R A N G

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;

                    var proses = s.FAR_GetDataBillingALL.Where(x => x.NoReg == noreg && x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.NoBukti.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBill = x.NoBukti,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{x.Jam:HH}:{x.Jam:mm}",
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Satuan = x.Satuan,
                        Harga = x.Harga,
                        QtyPesan = x.QtyPesan,
                        QtyTerpakai = x.QtyTerpakai,
                        Jumlah = x.Jumlah,
                        Dokter = x.Dokter,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E G I S T R A S I

        [HttpPost]
        public string ListLookupRegistrasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;
                    var proses = s.FAR_GetRegistrasi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {

                            proses = proses.Where(y =>
                                y.NoReg.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.Alamat.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                //y.Kamar.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value));
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {

                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.TglReg >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.TglReg <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoReg = x.NoReg,
                        Tanggal = x.TglReg.ToString("dd/MM/yyyy"),
                        RawatInap = x.RawatInap,
                        SectionAsal = x.SectionAsalID,
                        NRM = x.NRM,
                        Nama = x.NamaPasien,
                        Gender = x.JenisKelamin,
                        Perusahaan = x.Nama_Customer,
                        NoKartu = x.NoKartu,
                        Alamat = x.Alamat,
                        UmurTahun = x.UmurThn,
                        UmurBulan = x.UmurBln,
                        BeratBadan = x.BB,

                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B I L L I N G

        [HttpPost]
        public string ListLookupBilling(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;
                var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_GetDataBillingHeader.Where(x => x.NoReg == noreg && x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.JenisKerjasama.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{m.Jam:HH}:{m.Jam:mm}",
                        InputTanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                        NoReg = m.NoReg,
                        NRM = m.NRM,
                        Nama = m.NamaPasien,
                        JenisKerjasama = m.JenisKerjasama,
                        Perusahaan = m.Nama_Customer
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailBilling(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.FAR_GetDataBillingDetail.Where(x => x.NoBukti == id).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            QtyPesan = x.QtyPesan,
                            QtyTerpakai = x.QtyTerpakai,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}