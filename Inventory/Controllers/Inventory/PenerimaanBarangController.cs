﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PenerimaanBarangController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            var model = new PenerimaanBarangViewModel();
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            using (var s = new SIMEntities())
            {
                model.PPNNilai = 11;
                var setup = s.SetupAwal.FirstOrDefault(x => x.SetupName == "PPNPersen");
                if (setup != null)
                {
                    model.PPNNilai = setup.Nilai.ToDecimal();
                }
            }
            ViewBag.PPNNilai = model.PPNNilai;
            return View(model);
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            var status = filter.FirstOrDefault(x => x.Key == "Open").Value == "Open" ? "Open" : "Realisasi";
            if (status == "Open")
                return _listopen(orderby, orderbytype, pagesize, pageindex, filter);
            else
                return _listrealisasi(orderby, orderbytype, pagesize, pageindex, filter);
        }

        public string _listopen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter) 
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                    orderby = orderby == "No_DO" ? "No_Order" :
                    orderby == "Nama_Supplier" ? "Nama_Supplier" :
                    orderby == "Tanggal" ? "Tgl_Order" :
                    orderby == "No_Penerimaan" ? "No_Order" :
                    "";

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPenerimaanBarang.Where(x => x.Gudang_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Order.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.No_Order.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Order.ToString("yyyy-MM-dd"),
                        No = m.No_Order,
                        NoDO = "",
                        Realisasi = false,
                        OrderId = m.Order_ID,
                        Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string _listrealisasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPenerimaanBarangRealisasi.Where(x => x.GudangID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_DO.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.No_Penerimaan.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                        No = m.No_Penerimaan,
                        NoDO = m.No_DO,
                        Realisasi = true,
                        Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PenerimaanBarangViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<PenerimaanBarangDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertPenerimaanBarang(
                                model.Tanggal,
                                model.Potongan,
                                model.DP,
                                model.Remak,
                                model.OngkosKirim,
                                model.NoDO,
                                model.SupplierId,
                                model.OrderId,
                                model.Total,
                                model.LokasiId,
                                model.JatuhTempo,
                                model.IncludePPN,
                                model.AutoDO,
                                model.PPNNilai,
                                model.NoPenerimaanManual,
                                userid
                            ).FirstOrDefault() ?? 0;

                            if (id == 0) throw new Exception("INV_InsertPermintaan tidak mendapatkan nobukti");
                            
                            foreach (var x in model.Detail)
                            {
                            //if (x.TanggalExp == null) throw new Exception("Tanggal ED Obat Tidak Boleh Kosong");
                                var jum = (decimal)x.Qty * x.Harga;
                                var diskon = jum / 100 * (decimal)x.Disc;

                                s.INV_InsertPenerimaanBarangDetail(
                                    x.Harga,
                                    x.Disc,
                                    x.Qty,
                                    x.TanggalExp,
                                    (int)id,
                                    x.Id,
                                    diskon,
                                    x.NoBatch,
                                    model.OrderId,
                                    model.LokasiId
                                );
                            }
                            s.SaveChanges();
                        }
                        if (_process == "EDIT")
                        {
                            s.INV_UpdatePenerimaanBarang(
                                model.NoPenerimaan,
                                model.PPNNilai,
                                model.Total
                            );
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.INV_InsertPenerimaanBarangDelete((int)model.Id);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string DetailOrder(decimal id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_GetPOHeader.FirstOrDefault(x => x.Order_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_GetPODetail.Where(x => x.Order_ID == m.Order_ID);
                    var sectionlogin = Request.Cookies["Inventory_Section_Id"].Value;
                    var res = d.OrderBy(sectionlogin == "SEC031" ? "Kode_Barang ASC" : "Nama_Barang ASC").ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            IdSupplier = m.Supplier_ID,
                            OrderId =m.Order_ID,
                            SupplierNama = m.Nama_Supplier,
                            NoPenerimaanManual = m.NoPenerimaanManual,
                            NoPo = m.PO,
                            TanggalOrder = m.Tgl_Order.ToString("yyyy-MM-dd")
                        },
                        Detail = res.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            QtyOrder = x.Qty_Order,
                            QtyBeli = x.Qty_Tlh_Dibeli,
                            QtyTerima = x.QtyTerima,
                            Harga = x.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListPenerimaanBarangHeader.FirstOrDefault(x => x.No_Penerimaan == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListPenerimaanBarangDetail.Where(x => x.Penerimaan_ID == m.Penerimaan_ID);
                    var sectionlogin = Request.Cookies["Inventory_Section_Id"].Value;
                    var res = d.OrderBy(sectionlogin == "SEC031" ? "Kode_Barang ASC" : "Nama_Barang ASC").ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            SupplierId = m.Supplier_ID,
                            OrderId = m.Order_ID,
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tgl_Penerimaan.ToString("yyyy-MM-dd"),
                            NoPo = m.NoPO,
                            TanggalOrder = m.Tgl_Order == null ? "" : m.Tgl_Order.Value.ToString("yyyy-MM-dd"),
                            SupplierNama = m.Nama_Supplier,
                            Lokasi = m.Lokasi_ID,
                            NoDO = m.No_DO,
                            JatuhTempo = m.Tgl_JatuhTempo == null ? "" : m.Tgl_JatuhTempo.Value.ToString("yyyy-MM-dd"),
                            IncludePPN = m.IncludePPN,
                            PPNCheck = m.Pajak > 0 ? true : false,
                            PPNNilai = m.Pajak,
                            Remak = m.Keterangan,
                            NoPenerimaanManual = m.NoPenerimaanManual,
                            SubTotal = m.Total_Nilai,
                            Id = m.Penerimaan_ID
                        },
                        Detail = res.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Nama_Satuan,
                            QtyPO = x.Qty_PO,
                            TlhTerima = x.Qty_Telah_Terima,
                            QtyTerima = x.Qty_Penerimaan,
                            Harga = x.Harga,
                            Disc = x.Disc,
                            TanggalExp = x.Exp_Date == null ? "" : x.Exp_Date.Value.ToString("yyyy-MM-dd"),
                            NoBatch = x.NoBatch ==  null ? "" : x.NoBatch
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}