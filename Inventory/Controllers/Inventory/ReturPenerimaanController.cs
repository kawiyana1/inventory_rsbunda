﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class ReturPenerimaanController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListReturStok.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Retur.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(-1);
                            proses = proses.Where(y => y.Tgl_Retur >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(+2);
                            proses = proses.Where(y => y.Tgl_Retur <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Retur == null ? "" : m.Tgl_Retur.ToString("dd/MM/yyyy"),
                        No = m.No_Retur,
                        Nama_supplier = m.Nama_Supplier,
                        Keterangan = m.Keterangan,
                        Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, ReturPenerimaanViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<ReturPenerimaanDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertReturObat(
                                model.Keterangan,
                                model.Supplier,
                                lokasi,
                                model.PPN,
                                model.IncludePPN,
                                model.TotalNilai
                            ).FirstOrDefault() ?? 0;

                            if (id == 0) throw new Exception("INV_InsertReturObat tidak mendapatkan nobukti");
                            int idretur = int.Parse(id.ToString());
                            foreach (var x in model.Detail)
                            {
                                //if (x.TanggalExp == null) throw new Exception("Tanggal ED Obat Tidak Boleh Kosong");
                                if (x.Qty == 0) throw new Exception("Qty Tidak Boleh Kosong");
                                var jum = (decimal)x.Qty * x.Harga;
                                var diskon = jum / 100 * (decimal)x.Disc;

                                s.INV_InsertReturObatDetail(
                                    idretur,
                                    x.Harga,
                                    x.Id,
                                    x.Qty
                                );
                            }
                            s.SaveChanges();
                        }
                        //else if (_process == "DELETE")
                        //{
                        //    s.INV_InsertPenerimaanBarangDelete((int)model.Id);
                        //    s.SaveChanges();
                        //}

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            int lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListReturStokHeader.FirstOrDefault(x => x.No_Retur == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListReturStokDetail.Where(x => x.Retur_ID == m.Retur_ID).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.No_Retur,
                            Tanggal = m.Tgl_Retur.ToString("yyyy-MM-dd"),
                            Supplier_ID = m.Supplier_ID,
                            Nama_Supplier = m.Nama_Supplier,
                            Keterangan = m.Keterangan,
                            Lokasi = m.Lokasi_ID,
                            PPN = m.Pajak > 0 ? true : false,
                            PPNNilai = m.Pajak,
                            IncludePPN = m.IncludePPN,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.NamaSatuan,
                            Qty = x.Qty_Retur,
                            Harga = x.Harga_Retur,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_LookupBarang.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kategori.Contains(x.Value));
                        else if (x.Key == "Lokasi")
                        {
                            var _v = int.Parse(x.Value);
                            proses = proses.Where(y => y.Lokasi_ID == _v);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                        Stok = x.Qty_Stok,
                        Kategori = x.Kategori,
                        MinStok = x.Min_Stok,
                        MaxStok = x.Max_Stok,
                        QtyStok = x.Qty_Stok,
                        Harga = x.Harga_Beli
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}