﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiSetupDosisObatModel
    {
        public int Id { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public bool DosisDefault { get; set; }
    }
}