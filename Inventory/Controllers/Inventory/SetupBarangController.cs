﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class SetupBarangController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                var status = filter[1].Value;
                var statusbool = Convert.ToBoolean(status);
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.INV_mBarang(lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Satuan.Contains(x.Value) ||
                                y.Nama_KAtegori.Contains(x.Value) ||
                                y.KelompokJenis.Contains(x.Value)
                                );
                        }

                        if (x.Key == "Status" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                            y.Aktif == statusbool
                            );
                        }

                        if (x.Key == "SubKategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _x = int.Parse(x.Value);
                            proses = proses.Where(y =>
                            y.SubKategori_Id == _x
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Stok = x.Qty_Stok,
                        StokApotek = x.QtyUnitLain,
                        Aktif = (x.Aktif == true ? 1 : 0),
                        Satuan = x.Satuan,
                        Kategori = x.Nama_KAtegori,
                        Kategori_Id = x.Kategori_Id,
                        SubKategori_Id = x.SubKategori_Id,
                        Nama_Sub_Kategori = x.Nama_Sub_Kategori,
                        Jenis = x.KelompokJenis
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupBarangModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        if (model.HargaBeli == 0) throw new Exception("Detail Harga Beli Tentukan Terlebih Dahulu");
                        if (model.HargaPokok == 0) throw new Exception("Detail Harga Pokok Tentukan Terlebih Dahulu");
                        if (model.HargaRata2 == 0) throw new Exception("Detail Harga Rata - Rata Tentukan Terlebih Dahulu");
                        if (model.Kelompok == null || model.Kelompok == "") throw new Exception("Kelompok Jenis Tidak Boleh Kosong");
                        if (model.SatuanStok == null || model.SatuanStok == 0) throw new Exception("Satuan Stok");

                        //if (model.Supplier == null) throw new Exception("Lengkapi Harga, Supplier Dan Lainnya Di Detail Terlebih Dahulu");
                        if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                        var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                        var userid = User.Identity.GetUserId();
                        decimal? id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.INV_InsertBarang(
                                model.Kelas,
                                model.Nama,
                                model.PPN,
                                model.HargaBeli,
                                model.HargaPokok,
                                model.SatuanStok,
                                model.Konversi,
                                model.Aktif,
                                model.SatuanPembelian,
                                model.Kategori,
                                model.SubKategori,
                                model.Kelompok,
                                model.Golongan,
                                model.Jenis,
                                model.HargaRata2,
                                model.FormulariumJKN,
                                model.FormulariumUmum,
                                model.KelompokGrading,
                                model.CNOffFaktur,
                                model.CNOnFaktur,
                                model.Supplier,
                                model.HETcek,
                                model.HET,
                                model.AturanPakai_2
                                ).FirstOrDefault();
                            if (id == null) throw new Exception("Nobukti tidak ditemukan, INV_InsertBarang");
                            if(model.Detail == null) throw new Exception("Lokasi Masih Kosong, Silahkan Isi Lokasi");
                            model.Detail.Count();
                            if (model.Detail == null) model.Detail = new List<SetupBarangDetailModel>();
                            //var jmldetailsesuailokasi = model.Detail.Where(x => x.Id == lokasi);
                            //if (jmldetailsesuailokasi.Count() == 0)
                            //{
                            //    model.Detail.Add(new SetupBarangDetailModel()
                            //    {
                            //        Aktif = true,
                            //        Id = lokasi,
                            //        Min = 0,
                            //        Max = 0
                            //    });
                            //}
                            var lokasiSesuai = model.Detail.Where(x => x.Id == lokasi);
                            if(lokasiSesuai.Count() == 0)
                            {
                                throw new Exception("Lokasi Login Belum Dipilih");
                            }
                          
                            foreach (var m in model.Detail)
                            {
                                s.INV_InsertBarangLokasi((int)id, m.Id, m.Min, m.Max, m.Aktif, m.Hide);
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.INV_UpdatemBarang(
                                model.Id,
                                model.Nama,
                                model.Kategori,
                                model.SubKategori,
                                model.Kelas,
                                model.Golongan,
                                model.KelompokGrading,
                                model.FormulariumJKN,
                                model.FormulariumUmum,
                                model.HargaPokok,
                                model.CNOnFaktur,
                                model.CNOffFaktur,
                                model.Supplier,
                                model.Aktif,
                                model.HargaRata2,
                                model.HargaBeli,
                                model.Konversi,
                                model.SatuanPembelian,
                                model.Kelompok,
                                model.Jenis,
                                model.SatuanStok,
                                model.HETcek,
                                model.HET,
                                model.AturanPakai_2
                                );
                            id = model.Id;
                            s.SaveChanges();
                            var d = s.INV_GetmBarangLokasi(model.Id).ToList();
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.Lokasi_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.INV_InsertBarangLokasi(model.Id, x.Id, x.Min, x.Max, x.Aktif, x.Hide);
                                }
                                else
                                {
                                    // edit
                                    s.INV_UpdatemBarangLokasi(model.Id, x.Id, (double)x.Min, (double)x.Max, x.Aktif, x.Hide);
                                }

                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupBarang-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                    var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                    var m = s.INV_GetmBarangHeader.FirstOrDefault(x => x.Barang_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_GetmBarangLokasi(m.Barang_ID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Barang_ID,
                            Kelompok = m.Kelompok,
                            Jenis = m.KelompokJenis,
                            Kode = m.Kode_Barang,
                            Nama = m.Nama_Barang,
                            Aktif = (m.Aktif == true ? 1 : 0),
                            Kategori = m.Kategori_Id,
                            SubKategori = m.SubKategori_Id,
                            Kelas = m.Kelas_ID,
                            Golongan = m.GolonganID,
                            KelompokGrading = m.KelompokGrading,
                            FormulariumUmum = m.FormulariumUmum,
                            FormulariumJKN = m.FormulariumJKN,
                            SatuanPembelian = m.Beli_Satuan_Id,
                            HargaBeli = m.Harga_Beli,
                            PPN = m.PPn_Persen,
                            SatuanStok = m.Stok_Satuan_ID,
                            Konversi = m.Konversi,
                            HargaPokok = m.Harga_Pokok,
                            HargaRata2 = m.HRataRata,
                            CNOnFaktur = m.CNOnFaktur,
                            CNOffFaktur = m.CNOffFaktur,
                            Supplier = m.Supplier_ID,
                            JenisNama = m.KelompokJenis,
                            KategoriNama = m.Nama_Kategori,
                            SubKategoriNama = m.Nama_Sub_Kategori,
                            KelasNama = m.Nama_Kelas,
                            HET = m.NilaiHET,
                            HETcheck = (m.UseHET == true ? 1 : 0),
                            AturanPakai_2 = m.AturanPakai_2,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Lokasi_ID,
                            Section = x.Nama_Lokasi,
                            Min = x.Min_Stok,
                            Max = x.Max_Stok,
                            Qty = x.Qty_Stok,
                            Aktif = x.Aktif,
                            Hide = x.Hide
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupSection(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    //var _lokasi = int.Parse(filter.FirstOrDefault(x => x.Key == "Lokasi").Value);
                    var proses = s.INV_GetListLokasiBarang.Where(x => x.TipePelayanan != "Gudang" && x.StatusAktif == true || x.Lokasi_ID == lokasi).OrderBy(x => x.Nama_Lokasi).AsQueryable();
                    if (proses == null) throw new Exception("Sub Kategori Sudah Di SO");
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Nama_Lokasi.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Lokasi_ID,
                        Kode = m.Lokasi_ID,
                        Nama = m.Nama_Lokasi,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== REPORT
        
        #endregion
    }
}