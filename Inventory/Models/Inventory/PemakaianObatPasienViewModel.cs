﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PemakaianObatPasienViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public DateTime Jam { get; set; }
        public string SectionID { get; set; }
        public int UserID { get; set; }
        public string Keterangan { get; set; }
        public bool StatusBatal { get; set; }
        public string AlasanBatal { get; set; }
        public int UserIDBatal { get; set; }
        public DateTime TanggalBatal { get; set; }
        public DateTime JamBatal { get; set; }
        public string SectionPakai_ID { get; set; }
        public List<PemakaianObatPasienDetailViewModel> Detail { get; set; }
        public List<IsiKartuGudangFIFOViewModel> IsiFifo { get; set; }
    }

    public class PemakaianObatPasienDetailViewModel 
    {
        public string NoBukti { get; set; }
        public int BarangID { get; set; }
        public string Satuan { get; set; }
        public double QtyStok { get; set; }
        public double QtyPemakaian { get; set; }
        public decimal Harga { get; set; }
        public string Keterangan { get; set; }
    }

    public class IsiKartuGudangFIFOViewModel
    {
        public int LokasiID { get; set; }
        public int BarangID { get; set; }
        public string KodeSatuan { get; set; }
        public double Qty { get; set; }
        public double QtyPemakaian { get; set; }
        public decimal Harga { get; set; }
        public string NoBukti { get; set; }
        public string JenisTransaksi { get; set; }
        public string Penambahan { get; set; }
        public DateTime TglTransaksi { get; set; }
        public DateTime ExpDate { get; set; }
        public string JenisBarang { get; set; }
    }
}