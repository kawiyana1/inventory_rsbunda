﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class ReturPenerimaanViewModel
    {
        public string NoMutasi { get; set; }
        public string NoRetur { get; set; }
        public DateTime Tgl_Retur { get; set; }
        public string Keterangan { get; set; }
        public int Lokasi { get; set; }
        public int Supplier { get; set; }
        public decimal PPN { get; set; }
        public bool IncludePPN { get; set; }
        public decimal TotalNilai { get; set; }
        public List<ReturPenerimaanDetailViewModel> Detail { get; set; }
    }

    public class ReturPenerimaanDetailViewModel 
    {
        public int Qty { get; set; }
        public int Harga { get; set; }
        public int Id { get; set; }
        public int BarangID { get; set; }
        public int Disc { get; set; }
    }
}