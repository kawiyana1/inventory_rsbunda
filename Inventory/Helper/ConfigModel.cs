﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Inventory.Helper
{
    public static class ConfigModel
    {
        public static bool Develop { get { return false; } }
        public static string UnitName { get { return "Rumah Sakit Umum Bunda Negara"; } }
        public static string AppName { get { return "Inventory"; } }
        public static string Version { get { return "0.1"; } }
    }
}