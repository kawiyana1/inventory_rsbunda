﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PenerimaanBarangViewModel
    {
        public decimal Id { get; set; }
        public string NoPenerimaan { get; set; }
        public DateTime Tanggal { get; set; }
        public decimal Potongan { get; set; }
        public decimal DP { get; set; }
        public string Remak { get; set; }
        public string NoDO { get; set; }
        public int OrderId { get; set; }
        public int SupplierId { get; set; }
        public decimal OngkosKirim { get; set; }
        public decimal Total { get; set; }
        public int LokasiId { get; set; }
        public DateTime? JatuhTempo { get; set; }
        public bool IncludePPN { get; set; }
        public decimal PPNNilai { get; set; }
        public bool AutoDO { get; set; }
        public string NoPenerimaanManual { get; set; }

        public List<PenerimaanBarangDetailViewModel> Detail { get; set; }
    }

    public class PenerimaanBarangDetailViewModel 
    {
        public int Id { get; set; }
        public double Qty { get; set; }
        public DateTime? TanggalExp { get; set; }
        public string NoBatch { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
    }
}