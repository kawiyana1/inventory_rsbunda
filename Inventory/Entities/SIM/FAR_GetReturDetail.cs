//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class FAR_GetReturDetail
    {
        public string NoRetur { get; set; }
        public int Barang_ID { get; set; }
        public string Satuan { get; set; }
        public double Qty_Pesan { get; set; }
        public double Qty_Terpakai { get; set; }
        public double Qty_Retur { get; set; }
        public Nullable<decimal> HargaPersediaan { get; set; }
        public string NoBill { get; set; }
        public Nullable<int> JenisBarangID { get; set; }
        public Nullable<bool> PemakaianOnly { get; set; }
        public Nullable<decimal> Harga { get; set; }
        public Nullable<double> Disc { get; set; }
        public string Nama_Barang { get; set; }
        public string Kode_Barang { get; set; }
    }
}
