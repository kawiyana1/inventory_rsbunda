﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventory.Entities.SIM;
using Newtonsoft.Json;

namespace Inventory.Helper
{
    public static class HList
    {
        public static List<SelectListItem> LokasiLogin
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.INV_LoginGudang.Where(x => x.SectionID != "SECT122" && x.SectionID != "SECT138" && x.SectionID != "SECT101" && x.SectionID != "SECT129").OrderBy(x => x.GroupPelayanan).ToList();
                    foreach (var x in m.GroupBy(x => x.GroupPelayanan).ToList())
                    {
                        var group = new SelectListGroup()
                        {
                            Name = x.Key
                        };
                        foreach (var y in m.Where(z => z.GroupPelayanan == x.Key).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Text = y.SectionName,
                                Value = y.SectionID,
                                Group = group
                            });
                        }
                    };
                }
                return r;
            }
        }

        public static List<INV_LoginGudang> SectionLogin
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_LoginGudang.OrderBy(x => x.GroupPelayanan).ToList();
                    return m;
                }
            }
        }

        public static string Other_Section_Pelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_Poliklinik
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPoliklinikSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Gender
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = "" },
                    new SelectListItem(){ Text = "Male", Value = "M" },
                    new SelectListItem(){ Text = "Female", Value = "F" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string Hubungan
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = "" },
                    new SelectListItem(){ Text = "Istri/Suami", Value = "Istri/Suami" },
                    new SelectListItem(){ Text = "Anak", Value = "Anak" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string Hobby
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.mHobby.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Hobby}\">{x.Hobby}</option>";
                    }
                    return r;
                }
            }
        }

        public static string KategoriVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKategoriVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode_Kategori}\">{x.Kategori_Name}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SpesialisVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSpesialisVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SpesialisID.ToString()}\">{x.SpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SubSpesialisVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSubSpesialisVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SubSpesialisID.ToString()}\">{x.SubSpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_KelompokSection
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKelompokSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_TipePelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetTipePelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_List_UnitBisnis
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetUnitBisnisSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.UnitBisnisID}\">{x.UnitBisnisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Supplier
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListSupplier.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Supplier_ID.ToString()}\">{x.Nama_Supplier}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SetupBarangLokasi
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_LookupLokasi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Lokasi_ID.ToString()}\">{x.Nama_Lokasi}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SetupSubKategori
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mSubKategori.OrderBy(x => x.Nama_Sub_Kategori).ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SubKategori_ID.ToString()}\">{x.Nama_Sub_Kategori}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SetupSubKategoriFilter
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mSubKategori.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SubKategori_ID.ToString()}\">{x.Nama_Sub_Kategori}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SetupKategori
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mSubKategori.OrderBy(x => x.Nama_Sub_Kategori).ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SubKategori_ID.ToString()}\">{x.Nama_Sub_Kategori}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SetupFarmasiObatGenerik
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_mGenerik.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Generik_ID.ToString()}\">{x.Generik_Name}</option>";
                    }
                    return r;
                }
            }
        }



        public static string ReportKelompok
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "OBAT", Value = "OBAT" },
                    new SelectListItem(){ Text = "UMUM", Value = "UMUM" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ReportStatus
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "FAST MOVING", Value = "FAST MOVING" },
                    new SelectListItem(){ Text = "SLOW MOVING", Value = "SLOW MOVING" },
                    new SelectListItem(){ Text = "DEATH STOCK", Value = "DEATH STOCK" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string InventorySetupJenisKelompok
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "OBAT", Value = "OBAT" },
                    new SelectListItem(){ Text = "UMUM", Value = "UMUM" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string InventorySetupJenisPengadaan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_LookupKelompokPR.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelompokPR_ID}\">{x.KelompokPR}</option>";
                    }
                    return r;
                }
            }
        }

        public static string InventorySetupBarangGolongan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mGolongan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.GolonganID}\">{x.NamaGolongan}</option>";
                    }
                    return r;
                }
            }
        }

        public static string InventorySetupBarangKelompokGrading
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mKelompokGrading.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelompokGrading}\">{x.KelompokGrading}</option>";
                    }
                    return r;
                }
            }
        }

        public static string InventorySetupBarangSatuan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_mSatuan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Satuan_ID}\">{x.Nama_Satuan}</option>";
                    }
                    return r;
                }
            }
        }

        public static string InventorySetupBarangSupplier
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_GetSupplier.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Supplier_ID}\">{x.Kode_Supplier} - {x.Nama_Supplier}</option>";
                    }
                    return r;
                }
            }
        }

        public static string InventorySetupJenisAkun
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_GetAkun.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Akun_ID}\">{x.Akun_Name}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiBillingTipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_ListJenisKerjasama.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiTipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_TipePasien.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiResepRacikan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_GetRacikan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.NamaRacikan}\">{x.NamaRacikan}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiResepSection
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_GetSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string LokasiReportInfoStok
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangAmprahanDari.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Lokasi_ID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string AmprahanDari
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangAmprahanDari.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PembelianObatLuarSection
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_GetSectionObatLuar.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PembelianObatVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode_Supplier}\">{x.Kode_Supplier} - {x.Nama_Supplier}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PembelianObatDokter
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListDokter.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.DokterID}\">{x.DokterID} - {x.NamaDokter}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiBillingDokter
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_GetDokter.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-spesialis=\"{x.SpesialisName ?? "" }\" data-nama=\"{x.NamaDOkter ?? "" }\" value=\"{x.DokterID}\">{x.DokterID} - {x.NamaDOkter}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PembelianObatTipePembayaran
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "TUNAI", Value = "TUNAI" },
                    new SelectListItem(){ Text = "KREDIT", Value = "KREDIT" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string FilterRIRJ
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "ALL", Value = "" },
                    new SelectListItem(){ Text = "Resep RJ", Value = "Resep RJ" },
                    new SelectListItem(){ Text = "Resep RI", Value = "Resep RI" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string FilterStatus
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "All", Value = ""},
                    new SelectListItem(){ Text = "Aktif", Value = "true" },
                    new SelectListItem(){ Text = "Non Aktif", Value = "false"},
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string PembelianObatAlasan
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "OBAT BARU", Value = "OBAT BARU" },
                    new SelectListItem(){ Text = "STOK HABIS", Value = "STOK HABIS" },
                    new SelectListItem(){ Text = "OBAT TIDAK DISTOK", Value = "OBAT TIDAK DISTOK" },
                    new SelectListItem(){ Text = "GUDANG KOSONG", Value = "GUDANG KOSONG" },
                    new SelectListItem(){ Text = "GUDANG TUTUP", Value = "GUDANG TUTUP" },
                    new SelectListItem(){ Text = "SUDAH DIAUDIT", Value = "SUDAH DIAUDIT" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static List<SelectListItem> ListBulan
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Januari", Value = "1" },
                    new SelectListItem(){ Text = "Febuari", Value = "2" },
                    new SelectListItem(){ Text = "Maret", Value = "3" },
                    new SelectListItem(){ Text = "April", Value = "4" },
                    new SelectListItem(){ Text = "Mei", Value = "5" },
                    new SelectListItem(){ Text = "Juni", Value = "6" },
                    new SelectListItem(){ Text = "Juli", Value = "7" },
                    new SelectListItem(){ Text = "Agustus", Value = "8" },
                    new SelectListItem(){ Text = "September", Value = "9" },
                    new SelectListItem(){ Text = "Oktober", Value = "10" },
                    new SelectListItem(){ Text = "November", Value = "11" },
                    new SelectListItem(){ Text = "Desember", Value = "12" },
                };
            }
        }

        public static string ListKeteranganSO
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "KOREKSI STOK", Value = "KOREKSI STOK" },
                    new SelectListItem(){ Text = "OPNAME BULANAN", Value = "OPNAME BULANAN" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string Bulan
        {
            get
            {
                string r = "";
                foreach (var x in ListBulan)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string AmprahanKelompokPR(int lokasi)
        {

            using (var s = new SIMEntities())
            {
                var m = s.INV_LookupKelompokPR.Where(x => x.Lokasi_ID == lokasi).ToList();
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.KelompokPR_ID}\">{x.KelompokPR}</option>";
                }
                return r;
            }
        }

        public static string AmprahanKelompokPRAMprah
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_LookupKelompokPR.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.KelompokPR_ID}\" value=\"{x.KelompokPR_ID}\">{x.KelompokPR}</option>";
                    }
                    return r;
                }
            }
        }

        public static string AmprahanKepada
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangAmprahanKepada.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionId}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PeminjamanPermintaanKepada
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListSectionPinjam.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionId}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string AmprahanKepadaInsert
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangAmprahanKepada_Insert.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionId}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string OpnameSection
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option data-lokasi=\"{x.Lokasi_ID}\" value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PenerimaanMataUang
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListMataUang.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Currency_ID}\">{x.Currency_Name}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PurchasingOrderJenis
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListJenis.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Jenis}\">{x.Jenis}</option>";
                    }
                    return r;
                }
            }
        }

        public static string OpnameKelompokJenis
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListKelompokJenis.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelompokJenis}\">{x.KelompokJenis}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiAturanPakai1
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmDosisObat.ToList();
                    var l = new List<SelectListItem>()
                    {
                        new SelectListItem(){ Text = "", Value = "" },
                    };
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Dosis}\">{x.Dosis}</option>";
                    }
                    return r;
                }
            }
        }

        public static string FarmasiAturanPakai2
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.mBarang.ToList();
                    var l = new List<SelectListItem>()
                    {
                        new SelectListItem(){ Text = "", Value = "" },
                    };
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.AturanPakai_2}\">{x.AturanPakai_2}</option>";
                    }
                    return r;
                }
            }
        }
    }
}