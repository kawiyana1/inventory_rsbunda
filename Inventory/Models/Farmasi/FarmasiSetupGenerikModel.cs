﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiSetupGenerikModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
    }
}