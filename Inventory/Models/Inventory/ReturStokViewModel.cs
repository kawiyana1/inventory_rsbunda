﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class ReturStokViewModel
    {
        public DateTime Tanggal { get; set; }
        public int Supplier { get; set; }
        public int Lokasi { get; set; }
        public string Keterangan { get; set; }
        public decimal PPN { get; set; }
        public bool IncludePPN { get; set; }
        public decimal TotalNilai { get; set; }
        public List<ReturStokDetailViewModel> Detail { get; set; }
    }

    public class ReturStokDetailViewModel 
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public decimal Harga { get; set; }
    }
}