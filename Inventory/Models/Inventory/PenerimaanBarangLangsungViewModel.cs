﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PenerimaanBarangLangsungViewModel
    {
        public int Id { get; set; }
        public string No { get; set; }
        public DateTime? Tanggal { get; set; }
        public decimal Potongan { get; set; }
        public decimal DP { get; set; }
        public string Remak { get; set; }
        public decimal OngkosKirim { get; set; }
        public int SupplierId { get; set; }
        public int OrderId { get; set; }
        public int LokasiId { get; set; }
        public DateTime JatuhTempo { get; set; }
        public bool AutoDO { get; set; }
        public string NoPenerimaanManual { get; set; }
        public string NoDO { get; set; }
        public int Supplier { get; set; }
        public int Lokasi { get; set; }
        public int MataUang { get; set; }
        public bool PPN { get; set; }
        public decimal PPNNilai { get; set; }
        public decimal Total { get; set; }
        public List<PenerimaanBarangLangsungDetailViewModel> Detail { get; set; }
    }

    public class PenerimaanBarangLangsungDetailViewModel
    {
        public int Id { get; set; }
        public DateTime? TanggalExp { get; set; }
        public string NoBatch { get; set; }
        public int Qty { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
    }
}