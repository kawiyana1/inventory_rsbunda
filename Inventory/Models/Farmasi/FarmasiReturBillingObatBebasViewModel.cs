﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiReturBillingObatBebasViewModel
    {
        public string NoRetur { get; set; }
        public string NoBilling { get; set; }
        public DateTime Tanggal { get; set; }
        public string NoReg { get; set; }
        public string Alasan { get; set; }
        public int SubTotal { get; set; }
        public List<FarmasiReturBillingObatBebasDetailViewModel> Detail { get; set; }
    }

    public class FarmasiReturBillingObatBebasDetailViewModel 
    {
        public int Id { get; set; }
        public string NoBilling { get; set; }
        public string Satuan { get; set; }
        public double QtryRetur { get; set; }
        public decimal HargaQtryRetur { get; set; }
        public decimal Harga { get; set; }
    }
}