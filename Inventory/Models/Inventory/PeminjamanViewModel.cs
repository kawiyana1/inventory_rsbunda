﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PeminjamanViewModel
    {
        public string No { get; set; }
        public DateTime Tanggal { get; set; }
        public string NoDO { get; set; }
        public int Supplier { get; set; }
        public int Lokasi { get; set; }
        public int MataUang { get; set; }
        public decimal PPN { get; set; }
        public decimal PPNNilai { get; set; }
        public decimal Total { get; set; }
        public string Keterangan { get; set; }
        public List<PeminjamanDetailViewModel> Detail { get; set; }
    }

    public class PeminjamanDetailViewModel
    {
        public int Id { get; set; }
        public string KodeSatuan { get; set; }
        public string NamaBarang { get; set; }
        public int Qty { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
    }
}