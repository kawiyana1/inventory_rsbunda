﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class PembelianObatLuarViewModel
    {
        public DateTime Tanggal { get; set; }
        public string Section { get; set; }
        public string Vendor { get; set; }
        public string TipePembayaran { get; set; }
        public string Alasan { get; set; }
        public string Dokter { get; set; }
        public List<PembelianObatLuarDetailViewModel> Detail { get; set; }
    }

    public class PembelianObatLuarDetailViewModel 
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public decimal Harga { get; set; }
        public string NamaBarang { get; set; }
        public int Satuan_ID { get; set; }
    }
}