﻿//using Inventory.Helper;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace Inventory.Models
//{
//    public class ReportViewModel
//    {
//        public string Name { get; set; }
//        public string Category { get; set; }
//        public List<HReportModel> Reports { get; set; }
//    }
//}


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Inventory.Helper;

namespace Inventory.Models
{
    public class ReportViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime Start { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime End { get; set; }
        public int BarangID { get; set; }
        [Required]
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }

        public string JenisReport { get; set; }
        public string DokterID { get; set; }

        public string SPName { get; set; }
    }

    public class ReportInventoryViewModel
    {
        public string Name { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string Category { get; set; }

        public int LokasiID { get; set; }
        public List<HReportModel> Reports { get; set; }
    }
}