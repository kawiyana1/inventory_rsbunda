//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_ListReturStokHeader
    {
        public short Supplier_ID { get; set; }
        public decimal Retur_ID { get; set; }
        public System.DateTime Tgl_Retur { get; set; }
        public string No_Retur { get; set; }
        public string Keterangan { get; set; }
        public Nullable<short> User_ID { get; set; }
        public System.DateTime Tgl_Update { get; set; }
        public bool Status_Batal { get; set; }
        public bool Posting_KG { get; set; }
        public bool Posting_GL { get; set; }
        public bool Diakui_Pengurangan { get; set; }
        public bool ATK { get; set; }
        public bool Posted { get; set; }
        public Nullable<short> Lokasi_ID { get; set; }
        public Nullable<bool> Pembelian_Asset { get; set; }
        public string UserIDWeb { get; set; }
        public Nullable<decimal> Pajak { get; set; }
        public Nullable<bool> IncludePPN { get; set; }
        public Nullable<decimal> Total_Nilai { get; set; }
        public string Nama_Lokasi { get; set; }
        public string Nama_Supplier { get; set; }
    }
}
