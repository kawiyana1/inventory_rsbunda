﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    public class PostingKeKeuanganController : Controller
    {
        private string tipepelayanan = "GUDANG";

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(DateTime? start, DateTime? end)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPosting(lokasi.ToString()).AsQueryable();
                    if (start != null) proses = proses.Where(x => x.TglTransaksi >= start.Value);
                    if (end != null) proses = proses.Where(x => x.TglTransaksi <= end.Value);
                    var models = proses.ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoTransaksi = m.NoTransaksi,
                        Tanggal = m.TglTransaksi == null ? "" : m.TglTransaksi.Value.ToString("dd/MM/yyyy"),
                        Section = m.SectionName,
                        Jenis = m.JenisTransaksi,
                        Nilai = m.Nilai,
                        Keterangan = m.Keterangan,
                        Supplier = m.Nama_Supplier,
                        CN = ""
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Save(List<PostingKeKeuanganViewModel> model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                //using (var dbContextTransaction = s.Database.BeginTransaction())
                //{
                    try
                    {
                        if (model == null) model = new List<PostingKeKeuanganViewModel>();
                        foreach (var x in model)
                            s.INV_InsertPostingHeader(x.No);

                        s.SaveChanges();
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"PostingKeKeuangan-PROSES;".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        //dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true });
                    }
                    catch (SqlException ex) { 
                        //dbContextTransaction.Rollback(); 
                        return HConvert.Error(ex); 
                    }
                    catch (Exception ex) { 
                        //dbContextTransaction.Rollback(); 
                        return HConvert.Error(ex); 
                    }
                //}
            }
        }
    }
}