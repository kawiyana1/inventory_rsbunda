﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiReturBillingObatBebasController : Controller
    {
        // GET: FarmasiReturBillingObatBebas
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.VW_ListRETURBillingObatBebas.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoRetur.Contains(x.Value) ||
                                y.NoBill_ObatBebas.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value) 
                            );
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(-1);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(+2);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBill_ObatBebas = m.NoBill_ObatBebas,
                        NoRetur = m.NoRetur,
                        Keterangan = m.Keterangan,
                        Tanggal = m.Tanggal.ToString("yyyy/MM/dd"),
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiReturBillingObatBebasViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        string cekretur = "";
                        if (_process == "CREATE")
                        {
                            cekretur = s.FAR_ReturObatBebas(model.NoBilling).FirstOrDefault();
                            if (cekretur == "Proses Retur Tidak Bisa Dilakukan, Karena Pasien Telah Melakukan Pembayaran Dikasir") throw new Exception(cekretur);
                            if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                            var lokasiid = Request.Cookies["Inventory_Section_LokasiId"].Value;
                            var section = Request.Cookies["Inventory_Section_Id"].Value;
                            if (model.Detail == null) model.Detail = new List<FarmasiReturBillingObatBebasDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            if (model.SubTotal == 0) throw new Exception("Total Qty Retur Boleh Kosong");
                            id = s.FAR_InsertReturObatBebasHeader(
                                model.NoBilling,
                                model.Tanggal,
                                model.Tanggal,
                                section,
                                model.Alasan,
                                null,
                                null,
                                userid
                            ).FirstOrDefault();
                          

                            if (string.IsNullOrEmpty(id)) throw new Exception("FAR_InsertReturHeader tidak mendapatkan nobukti");
                            foreach (var x in model.Detail)
                            {
                                var hargaretur = (decimal)x.QtryRetur * x.Harga;
                                //if(x.QtryRetur == 0) throw new Exception("Qty Retur Tidak Boleh 0");
                                s.FAR_InsertReturFarmasiDetail_obatBebas(
                                    id,
                                    x.Id,
                                    x.Satuan,
                                    x.QtryRetur,
                                    hargaretur,
                                    x.NoBilling,
                                    x.Harga,
                                    int.Parse(lokasiid)
                                );
                            }
                            //s.HitungEstimasiBiaya(model.NoReg);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiRetur-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.VW_ListRETURBillingObatBebas.FirstOrDefault(x => x.NoRetur == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.VW_ListBillingObatBebas_Detail.Where(x => x.NoRetur == m.NoRetur).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoRetur = m.NoRetur,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            TanggalBilling = m.Tanggal.ToString("yyyy-MM-dd"),
                            Jam = $"{m.Tanggal:HH}:{m.Tanggal:mm}",
                            JamBilling = $"{m.Tanggal:HH}:{m.Tanggal:mm}",
                            Alasan = m.Keterangan == null ? "" : m.Keterangan,
                            NoBilling = m.NoBill_ObatBebas
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Qty_Retur = x.Qty_Retur,
                            NoBill = x.NoBill_ObatBebas,
                            HargaPersediaan = x.HargaPersediaan,
                            Harga = x.Harga,
                            JumlahObat = x.JmlObat
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B A R A N G

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;

                    var proses = s.FAR_GetDataBillingALL.Where(x => x.NoReg == noreg && x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.NoBukti.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBill = x.NoBukti,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{x.Jam:HH}:{x.Jam:mm}",
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Satuan = x.Satuan,
                        Harga = x.Harga,
                        QtyPesan = x.QtyPesan,
                        QtyTerpakai = x.QtyTerpakai,
                        Jumlah = x.Jumlah,
                        Dokter = x.Dokter,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B I L L I N G

        [HttpPost]
        public string ListLookupBilling(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;
                var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListBillingObatBebasHeader.Where(x => x.NoReg == noreg && x.SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoResep.Contains(x.Value) ||
                                y.NoBillHeader.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.JenisKerjasama.Contains(x.Value) ||
                                y.NoBukti.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            //var d = DateTime.Parse(x.Value).AddDays(-1);
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            //var d = DateTime.Parse(x.Value).AddDays(+2);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{m.Jam:HH}:{m.Jam:mm}",
                        InputTanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                        JenisKerjasama = m.JenisKerjasama,
                        NamaPasien = m.NamaPasien,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailBilling(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.FAR_ListBillingObatBebasDetail.Where(x => x.NoBukti == id).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            JmlObat = x.JmlObat,
                            Harga = x.Harga,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}