﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiResepController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            if (filter.Find(x => x.Key == "Status").Value == "All")
                return _ListAll(orderby, orderbytype, pagesize, pageindex, filter);
            else if (filter.Find(x => x.Key == "Status").Value == "Open")
                return _ListOpen(orderby, orderbytype, pagesize, pageindex, filter);
            else
                return _ListClose(orderby, orderbytype, pagesize, pageindex, filter);
        }

        private string _ListAll(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListResepAll.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "ResepFilter" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JenisResep.Contains(x.Value)
                            );
                        }
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoResep.Contains(x.Value) ||
                                y.NoRegistrasi.Contains(x.Value) ||
                                y.SectionAsal.Contains(x.Value) ||
                                y.NamaDOkter.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.Gender.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.JenisPasien.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBill,
                        NoResep = m.NoResep,
                        //Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                        NoRegistrasi = m.NoRegistrasi,
                        Section = m.SectionAsal,
                        Nama = m.NamaPasien,
                        Gender = m.Gender,
                        IdDokter = m.DokterID,
                        NamaDokter = m.NamaDOkter,
                        NRM = m.NRM,
                        JenisPasien = m.JenisPasien,
                        Perusahaan = m.Perusahaan,
                        Realisasi = m.Realisasi,
                        Cito = m.Cito,
                        JenisResep = m.JenisResep,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        private string _ListOpen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListResepOpen.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoResep.Contains(x.Value) ||
                                y.NoRegistrasi.Contains(x.Value) ||
                                y.SectionAsal.Contains(x.Value) ||
                                y.NamaDOkter.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.Gender.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.JenisPasien.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "ResepFilter" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JenisResep.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = "",
                        NoResep = m.NoResep,
                        //Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                        NoRegistrasi = m.NoRegistrasi,
                        Section = m.SectionAsal,
                        Nama = m.NamaPasien,
                        Gender = m.Gender,
                        IdDokter = m.DokterID,
                        NamaDokter = m.NamaDOkter,
                        NRM = m.NRM,
                        JenisPasien = m.JenisPasien,
                        Perusahaan = m.Perusahaan,
                        Realisasi = m.Realisasi,
                        Cito = m.Cito,
                        JenisResep = m.JenisResep,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        private string _ListClose(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListResepClose.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "ResepFilter" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JenisResep.Contains(x.Value)
                            );
                        }
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoResep.Contains(x.Value) ||
                                y.NoRegistrasi.Contains(x.Value) ||
                                y.SectionAsal.Contains(x.Value) ||
                                y.NamaDOkter.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.Gender.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.JenisPasien.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value) ||
                                y.Perusahaan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBill,
                        NoResep = m.NoResep,
                        //Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                        NoRegistrasi = m.NoRegistrasi,
                        Section = m.SectionAsal,
                        Nama = m.NamaPasien,
                        Gender = m.Gender,
                        IdDokter = m.DokterID,
                        NamaDokter = m.NamaDOkter,
                        NRM = m.NRM,
                        JenisPasien = m.JenisPasien,
                        Perusahaan = m.Perusahaan,
                        Realisasi = m.Realisasi,
                        Cito = m.Cito,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiResepViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string nobukti = "";

                        if (_process == "CREATE")
                        {
                            s.BPJS_Task_Insert(model.NoReg, "TaskNo_6", model.TanggalBukaResep, model.TanggalBukaResep, model.TanggalBukaResep);

                            DateTime tanggalsekarang = DateTime.Now;
                            s.BPJS_Task_Insert(model.NoReg, "TaskNo_7", tanggalsekarang, tanggalsekarang, tanggalsekarang);
                            var rspsave = s.FAR_GetResepHeader.FirstOrDefault(x => x.NoResep == model.NoResep);
                            var billsave = s.FAR_ListBillingResep.FirstOrDefault(x => x.NoResep == model.NoResep);
                            var transaksibill = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == rspsave.NoRegistrasi);
                            if (transaksibill.StatusBayar == "Sudah Bayar") throw new Exception("Pasiem sudah melalukan pembayaran");
                            if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                            var section = Request.Cookies["Inventory_Section_Id"].Value;

                            if (transaksibill.ProsesPayment == true) throw new Exception("Data pasien sedang di proses dikasir, silahkan hubungi kasir !!");
                            if (model.Detail == null) model.Detail = new List<FarmasiResepDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");

                            var cektelaah = s.Farmasi_trTelaahResepDetail.Where(x => x.NoBukti == model.NoResep).FirstOrDefault();
                            if (cektelaah == null) throw new Exception("Isi Kajian Resep Terlebih Dahulu");
                            var id = s.FAR_InsertBilling(
                                model.Tanggal,
                                model.Tanggal,
                                model.Paket,
                                model.KodePaket,
                                model.BiayaRacik,
                                model.Total,
                                section,
                                model.NoResep,
                                model.KelebihanPlafon,
                                model.Keterangan,
                                model.IncludeJasa,
                                userid
                            ).FirstOrDefault();
                            nobukti = id.NoBukti;
                            if (id.NoBukti == null || id.Realisasi == 1) throw new Exception("Gagal Karena Resep Sudah Realisasi");
                            //if (string.IsNullOrEmpty(id)) throw new Exception("FAR_InsertBilling tidak mendapatkan nobukti");
                            var A = model.Detail[0].ToString();
                            var qtylock = 0;
                            foreach (var x in model.Detail)
                            {
                                if (x.Obat > x.QtyStok && x.Lock == true)
                                {
                                    qtylock += 1;
                                }
                                s.FAR_InsertBillingDetail(
                                    id.NoBukti,
                                    x.Id,
                                    x.Obat,
                                    x.AturanPakai1,
                                    x.AturanPakai2,
                                    x.Harga,
                                    x.Disc,
                                    x.TanggalED,
                                    x.Racik,
                                    x.WaktuTiket1,
                                    x.WaktuTiket2,
                                    x.WaktuTiket3,
                                    x.WaktuTiket4,
                                    x.WaktuTiket5,
                                    x.WaktuTiket6,
                                    x.WaktuTiket7,
                                    x.WaktuTiket8,
                                    x.WaktuTiket9,
                                    x.WaktuTiket10,
                                    x.WaktuTiket11,
                                    x.WaktuTiket12
                                );
                            }
                            s.HitungEstimasiBiaya(model.NoReg);
                            if (qtylock > 0)
                            {
                                throw new Exception("Qty Stok Tidak Mencukupi");
                            }
                            s.SaveChanges();
                        }

                        if (_process == "CANCEL")
                        {
                            s.FAR_SIMtrResep_Batal(model.NoResep);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiResep-{_process}; id:{nobukti};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = nobukti });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string DetailModel(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_GetResepHeader.FirstOrDefault(x => x.NoResep == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.FAR_GetResepDetail.Where(x => x.NoResep == m.NoResep).OrderBy(x => x.Nama_Barang).OrderBy(x => x.JenisRacikan).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Dokter = m.NamaDOkter,
                            Spesialis = m.SpesialisName,
                            SectionAsal = m.SectionAsalID,
                            Phone_Reg = m.Phone_Reg,
                            PaketObat = m.KodePaket,
                            Paket = m.Paket ?? false,
                            IncludeJasa = m.IncludeJasa ?? false,

                            NoResep = m.NoResep,
                            ResepTanggal = m.TglResep.ToString("yyyy-MM-dd") == null ? "" : m.TglResep.ToString("yyyy-MM-dd"),
                            ResepJam = $"{m.JamResep.ToString("HH")}:{m.JamResep.ToString("mm")}",
                            Cito = m.Cyto,
                            EstimasiSisa = m.EstimasiSisa,

                            NoReg = m.NoRegistrasi,
                            NRM = m.NRM,
                            NamaPasien = m.NamaPasien,
                            Alamat = m.Alamat,
                            Gender = m.Gender,
                            UmurTahun = m.UmurThn,
                            UmurBulan = m.UmurBln,
                            BeratBadan = m.BeratBadan,
                            Kerjasama = m.JenisPasien,
                            NoKartu = m.NoKartu,

                            Keterangan = m.Keterangan,
                            QtyPuyer = m.QtyPuyer,
                            SatuanPuyer = m.SatuanPuyer,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Aturan1 = x.AturanPakai,
                            Aturan2 = x.AturanPakai_2,
                            TglED = "",
                            JumlahResep = x.JmlResep,
                            JumlahObat = x.JmlObat,
                            Stok = x.Stok,
                            Harga = x.Harga,
                            LockStock = (x.LockStock == 0 ? false : true),
                            Disc = 0,
                            Racik = x.Racik,
                            JenisRacikan = x.JenisRacikan,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_ListBillingResep.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.FAR_ListBillingResepDetail.Where(x => x.NoBukti == m.NoBukti).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                            Jam = $"{m.Jam.ToString("HH")}:{m.Jam.ToString("mm")}",
                            Dokter = m.NamaDOkter,
                            Spesialis = m.SpesialisName,
                            Phone_Reg = m.Phone_Reg,
                            SectionAsal = m.SectionAsalID,
                            KodePaket = m.KodePaket,
                            PaketObat = m.Paket ?? false,
                            Paket = m.PaketObat,
                            InclideJasa = m.IncludeJasa,
                            NoResep = m.NoResep,
                            TanggalResep = m.TglResep.Value.ToString("yyyy-MM-dd") == null ? "" : m.TglResep.Value.ToString("yyyy-MM-dd"),
                            JamResep = $"{m.TglResep.Value.ToString("HH")}:{m.TglResep.Value.ToString("mm")}",
                            Cito = m.Cito,
                            NoReg = m.NoReg,
                            NRM = m.NRM,
                            Nama = m.NamaPasien,
                            EstimasiSisa = m.EstimasiSisa,
                            Alamat = m.Alamat,
                            Gender = m.JenisKelamin,
                            UmurTahun = m.UmurThn,
                            UmurBulan = m.UmurBln,
                            BeratBadan = m.BeratBadan,
                            Kerjasama = m.JenisKerjasama,
                            NoKartu = m.NoKartu,
                            Keterangan = m.Keterangan,
                            BiayaRacik = m.BiayaRacik ?? 0,
                            KelebihanPlafon = m.TotalKelebihanPlafon ?? 0,
                            SudahTelaahResep = s.Farmasi_trTelaahResep_Check(m.NoResep).FirstOrDefault(),
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Aturan1 = x.Aturan1,
                            Aturan2 = x.Aturan2,
                            TglED = x.TglED == null ? "" : x.TglED.Value.ToString("yyyy-MM-dd"),
                            JumlahResep = x.JmlResep,
                            JumlahObat = x.JmlObat,
                            Harga = x.Harga,
                            Stok = x.Stok,
                            Disc = x.Disc,
                            Racikan = x.NamaRacikan,
                            Eresep1 = x.WaktuTiket1 ?? false,
                            Eresep2 = x.WaktuTiket2 ?? false,
                            Eresep3 = x.WaktuTiket3 ?? false,
                            Eresep4 = x.WaktuTiket4 ?? false,
                            Eresep5 = x.WaktuTiket5 ?? false,
                            Eresep6 = x.WaktuTiket6 ?? false,
                            Eresep7 = x.WaktuTiket7 ?? false,
                            Eresep8 = x.WaktuTiket8 ?? false,
                            Eresep9 = x.WaktuTiket9 ?? false,
                            Eresep10 = x.WaktuTiket10 ?? false,
                            Eresep11 = x.WaktuTiket11 ?? false,
                            Eresep12 = x.WaktuTiket12 ?? false,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - P A K E T

        [HttpPost]
        public string ListLookupPaket(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_GetPaketHeader.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.KodePaket.Contains(x.Value) ||
                                y.NamaPaket.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.KodePaket,
                        Nama = m.NamaPaket
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailPaket(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.FAR_GetPaketDetail.Where(x => x.KodePaket == id).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Nama_Satuan,
                            JumlahResep = x.JmlObat,
                            JumlahObat = x.JmlResep,
                            Stok = x.QtyStok,
                            Harga = x.Harga,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;

                    var proses = s.FAR_GetListObat(section, noreg).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.NamaBarang.Contains(x.Value) ||
                                y.Nama_Kategori.Contains(x.Value));
                    }
                    var transaksi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (transaksi.ProsesPayment == true) throw new Exception("Data pasien sedang di proses dikasir, silahkan hubungi kasir !!");
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    foreach (var m in models)
                    {
                        var h = s.FT_GetHarga_Barang_Pelayanan_Skalar(m.Barang_ID, section, "HargaJual", noreg).FirstOrDefault();
                        m.Harga_Jual = h ?? 0;
                    }

                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.NamaBarang,
                        Satuan = x.Satuan_Stok,
                        Stok = x.Qty_Stok,
                        Kategori = x.Nama_Kategori,
                        QtyStok = x.Qty_Stok,
                        LockStock = x.LockStock,
                        ProsesTransaksi = transaksi.ProsesPayment,
                        Harga = x.Harga_Jual ?? 0,
                        Aturan2 = x.AturanPakai_2,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P - T E L A A H  R E S E P

        [HttpGet]
        public string checkTelaahFarmasi(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var pesan = s.Farmasi_trTelaahResep_Check(nobukti).FirstOrDefault();
                    if (pesan == "Sudah Ada")
                    {
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahResep.ToList();
                        foreach (var x in m)
                        {
                            var dt_telaah = s.Farmasi_trTelaahResepDetail.Where(xx => xx.NoBukti == nobukti && xx.NoUrut == x.NoUrut).FirstOrDefault();
                            if (dt_telaah == null)
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = false,
                                    Keterangan = ""
                                });
                            }
                            else
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = dt_telaah.Iya,
                                    Keterangan = dt_telaah.Keterangan
                                });
                            }
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                    else
                    {
                        //return JsonConvert.SerializeObject(new
                        //{
                        //    IsSuccess = true,
                        //    Data = pesan,
                        //    Message = "-"
                        //});
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahResep.ToList();
                        foreach (var x in m)
                        {
                            dt_new.Add(new
                            {
                                NoUrut = x.NoUrut,
                                Telaah = x.Telaah,
                                Iya = true,
                                Keterangan = "-"
                            });
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpGet]
        public string dataTelaahFarmasi(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt = s.Farmasi_mTelaahResep.ToList();
                    var gettelaahfarmasi = s.Farmasi_trTelaahResepDetail.Where(x => x.NoBukti == nobukti).ToList();
                    if (dt != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string postTelaahFarmasi(string nobukti, List<detailTelaahFarmasi> option)
        {

            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {

                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var userid = User.Identity.GetUserId();
                    var dt_telaahcek = s.Farmasi_trTelaahResep.Where(x => x.NoBukti == nobukti).FirstOrDefault();
                    if (dt_telaahcek == null)
                    {
                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {

                            var dt = s.Farmasi_trTelaahResep_insert(nobukti, userid);
                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahResep.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahResepDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }
                    else
                    {

                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {
                            var Farmasi_trTelaahResepDetail = s.Farmasi_trTelaahResepDetail.Where(xx => xx.NoBukti == nobukti).ToList();
                            foreach (var xxx in Farmasi_trTelaahResepDetail)
                            {
                                s.Farmasi_trTelaahResepDetail.Remove(xxx);
                                s.SaveChanges();
                            }

                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahResep.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahResepDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }

                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion 

        #region ===== S E T U P - T E L A A H  O B A T  

        [HttpGet]
        public string checkTelaahObat(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var pesan = s.Farmasi_trTelaahObat_Check(nobukti).FirstOrDefault();
                    if (pesan == "Sudah Ada")
                    {
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahObat.ToList();
                        foreach (var x in m)
                        {
                            var dt_telaah = s.Farmasi_trTelaahObatDetail.Where(xx => xx.NoBukti == nobukti && xx.NoUrut == x.NoUrut).FirstOrDefault();
                            if (dt_telaah == null)
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = false,
                                    Keterangan = ""
                                });
                            }
                            else
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = dt_telaah.Iya,
                                    Keterangan = dt_telaah.Keterangan
                                });
                            }
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = pesan,
                            Message = "-"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpGet]
        public string dataTelaahObat(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt = s.Farmasi_mTelaahObat.ToList();
                    var gettelaahfarmasi = s.Farmasi_trTelaahObatDetail.Where(x => x.NoBukti == nobukti).ToList();
                    if (dt != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string postTelaahObat(string nobukti, List<detailTelaahFarmasi> option)
        {

            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {

                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var userid = User.Identity.GetUserId();
                    var dt_telaahcek = s.Farmasi_trTelaahObat.Where(x => x.NoBukti == nobukti).FirstOrDefault();
                    if (dt_telaahcek == null)
                    {
                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {

                            var dt = s.Farmasi_trTelaahObat_insert(nobukti, userid);
                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahObat.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahObatDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }
                    else
                    {

                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {
                            var Farmasi_trTelaahObatDetail = s.Farmasi_trTelaahObatDetail.Where(xx => xx.NoBukti == nobukti).ToList();
                            foreach (var xxx in Farmasi_trTelaahObatDetail)
                            {
                                s.Farmasi_trTelaahObatDetail.Remove(xxx);
                                s.SaveChanges();
                            }

                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahObat.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahObatDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }

                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion 


    }
}