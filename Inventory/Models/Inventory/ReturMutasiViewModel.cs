﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class ReturMutasiViewModel
    {
        public string NoMutasi { get; set; }
        public DateTime Tanggal { get; set; }
        public string Keterangan { get; set; }
        public List<ReturMutasiDetailViewModel> Detail { get; set; }
    }

    public class ReturMutasiDetailViewModel 
    {
        public double Qty { get; set; }
        public int Id { get; set; }
    }
}