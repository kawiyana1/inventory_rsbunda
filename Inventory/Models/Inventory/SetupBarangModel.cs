﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class SetupBarangModel
    {
        public int Id { get; set; }
        public string Kelompok { get; set; }
        public string Jenis { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public bool Aktif { get; set; }
        public int? Kategori { get; set; }
        public int? SubKategori { get; set; }
        public int? Kelas { get; set; }
        public int? Golongan { get; set; }
        public string KelompokGrading { get; set; }
        public bool FormulariumUmum { get; set; }
        public bool FormulariumJKN { get; set; }
        public int? SatuanPembelian { get; set; }
        public decimal HargaBeli { get; set; }
        public int? PPN { get; set; }
        public int? SatuanStok { get; set; }
        public int? Konversi { get; set; }
        public decimal HargaPokok { get; set; }
        public decimal? HargaRata2 { get; set; }
        public int? CNOnFaktur { get; set; }
        public int? CNOffFaktur { get; set; }
        public int? Supplier { get; set; }
        public decimal HET { get; set; }
        public bool HETcek { get; set; }
        public string AturanPakai_2 { get; set; }
        public List<SetupBarangDetailModel> Detail { get; set; }
    }

    public class SetupBarangDetailModel 
    {
        public int Id { get; set; }
        public string Section { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public bool Aktif { get; set; }
        public bool Hide { get; set; }
    }
}