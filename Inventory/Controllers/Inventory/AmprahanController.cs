﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class AmprahanController : Controller
    {
        private string tipepelayanan = "GUDANG";
        private string tipepelayanan2 = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangAmprahan.AsQueryable();
                    if (Request.Cookies["Inventory_Section_Group"].Value == "GUDANG")
                        proses = proses.Where(x => x.LokasiTujuan == lokasi);
                    else
                        proses = proses.Where(x => x.SectionAsal == section);
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.SectionAsal.Contains(x.Value) ||
                                y.SectionAsal_SectionName.Contains(x.Value) ||
                                y.SectionTujuan.Contains(x.Value) ||
                                y.SectionTujuan_SectionName.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value));
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                   
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        SectionAsal = m.SectionAsal_SectionName,
                        SectionTujuan = m.SectionTujuan_SectionName,
                        Keterangan = m.Keterangan,
                        BelumMutasi = !m.Batal && !m.Disetujui,
                        SudahMutasi = !m.Batal && m.Disetujui,
                        BatalAmprah = m.Batal
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, AmprahanViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan &&
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var nobukti = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<AmprahanDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            nobukti = s.INV_InsertGudangAmprahan(
                                model.Tanggal,
                                model.Keterangan,
                                model.DariSection, 
                                model.Kepada,
                                userid
                            ).FirstOrDefault();
                            if (model.DariSection == model.Kepada) throw new Exception("Section Amprah dan Section Tujuan tidak boleh sama. Silahkan cek terlebih dahulu");
                            if (string.IsNullOrEmpty(nobukti)) throw new Exception("Nobukti tidak ditemukan, INV_InsertGudangAmprahan");
                            foreach (var m in model.Detail) {
                                s.INV_InsertGudangAmprahanDetail(nobukti, m.Id, m.Qty);
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<AmprahanDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            nobukti = model.NoBukti;
                            s.INV_UpdateGudangAmprahan(
                                model.NoBukti,
                                model.Keterangan
                            );

                            var d = s.INV_ListGudangAmprahanDetail.Where(x => x.NoBukti == model.NoBukti).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
                                //delete
                                if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.NoBukti, x.Barang_ID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.Barang_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.INV_InsertGudangAmprahanDetail(model.NoBukti, x.Id, x.Qty);
                                }
                                else
                                {
                                    // edit
                                    s.INV_UpdateGudangAmprahanDetail(model.NoBukti, x.Id, x.Qty);
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            nobukti = model.NoBukti;
                            s.INV_GD_trAmprahan_BATAL(model.NoBukti);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Amprahan-{_process}; id:{model.NoBukti};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = nobukti });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangAmprahanHeader.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListGudangAmprahanDetail.Where(x => x.NoBukti == m.NoBukti).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            DariSection = m.SectionAsal,
                            Kepada = m.SectionTujuan,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Konversi = x.Konversi,
                            Kategori = x.Nama_Kategori,
                            Harga = x.Harga,
                            MinQty = x.Min_Stock,
                            MaxQty = x.Max_Stock,
                            QtyStok = x.QtyStok,
                            Satuan = x.Satuan,
                            QtyAsalSection = x.QtyRealisasiPertama,
                            Qty = x.Qty
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P - P R

        [HttpPost]
        public string SavePR(string _process, AmprahanPRViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<AmprahanPRDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");

                            id = s.INV_InsertPermintaan(
                                //model.NoBukti,
                                model.Tanggal,
                                model.Gudang,
                                model.TanggalDibutuhkan,
                                model.KelompokPR,
                                model.Keterangan,
                                model.Total,
                                model.SupplierID,
                                userid).FirstOrDefault() ?? 0;

                            if (id == 0) throw new Exception("INV_InsertPermintaan tidak mendapatkan id");

                            foreach (var x in model.Detail)
                            {
                                s.INV_InsertPermintaanDetail(
                                    (int)id,
                                    x.Id,
                                    x.Qty,
                                    x.Harga,
                                    model.Gudang
                                );
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Amprahan-PR-CREATE; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                
                int totalcount;
                using (var s = new SIMEntities())
                {
                    
                    var getlokasi = filter.FirstOrDefault(x => x.Key == "Lokasi").Value;
                    var getlokasitujuanid = filter.FirstOrDefault(x => x.Key == "LokasiID").Value; 
                    var getsubkategoriid = filter.FirstOrDefault(x => x.Key == "SubKategori").Value; 
                    int lokasitujuanid = int.Parse(getlokasi);
                    int darilokasi = int.Parse(getlokasitujuanid);
                    var proses = s.INV_GetmBarangAmprah(darilokasi, lokasitujuanid).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value)) { 
                            proses = proses.Where(y =>
                            y.Nama_Barang.Contains(x.Value) ||
                            y.Kategori.Contains(x.Value));
                        }

                        if (x.Key == "SubKategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _x = int.Parse(x.Value);
                            proses = proses.Where(y =>
                            y.SubKategori_ID == _x
                            );
                        }

                        //else if (x.Key == "Lokasi") {
                        //    var _v = int.Parse(x.Value);
                        //    proses = proses.Where(y => y.Asal_LokasiID == _v);
                        //}
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        IDKategori = x.SubKategori_ID,
                        NamaKategori = x.Nama_Sub_Kategori,
                        Satuan = x.Satuan,
                        Stok = x.Tujuan_QtyStok,
                        Kategori = x.Kategori,
                        MinStok = x.Asal_MinStok,
                        MaxStok = x.Asal_MaxStok,
                        QtyStok = x.Asal_QtyStok,
                        Harga = x.Harga_Beli
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}