﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiBillingController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListBilling.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.NamaDokter.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {

                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        //Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                        Jam = $"{m.Jam.ToString("HH")}:{m.Jam.ToString("mm")}",
                        NoReg = m.Registrasi_NoReg,
                        NRM = m.NRM,
                        Nama = m.NamaPasien,
                        Gender = m.Registrasi_JenisKelamin,
                        Kerjasama = m.Kerjasama_JenisKerjasama,
                        Perusahaan = m.Registrasi_Nama_Customer == null ? "" : m.Registrasi_Nama_Customer,
                        Dokter = m.NamaDokter,
                        NoKartu = m.Registrasi_NoKartu,
                        Spesialis = m.Dokter_SpesialisName,
                        Section = m.SectionAsal_SectionName,
                        NoResep = m.Resep_NoResep,
                        Batal = m.Batal,
                        Audit = m.IncomeAudit,
                        PasienLoyal = m.PasienLoyal ?? false,
                        PasienBermasalah = m.PasienBermaslah ?? false,
                        User = ""
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiBillingViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            //var rspsave = s.SIMmSection.FirstOrDefault(x => x.NoReg == model.NoReg);
                            //if (rspsave == null) throw new Exception("No Reg Tidak Ada");
                            var transaksibill = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == model.NoReg);
                            if (transaksibill.StatusBayar == "Sudah Bayar") throw new Exception("Pasiem sudah melalukan pembayaran");
                            var tanggalsekarang = DateTime.Now;
                            var transaksi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == model.NoReg);
                            if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                            var section = Request.Cookies["Inventory_Section_Id"].Value;
                            if (transaksi.ProsesPayment == true) throw new Exception("Data pasien sedang di proses dikasir, silahkan hubungi kasir !!");
                            if (model.Detail == null) model.Detail = new List<FarmasiBillingDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");

                            s.BPJS_Task_Insert(model.NoReg, "TaskNo_7", tanggalsekarang, tanggalsekarang, tanggalsekarang);
                            id = s.FAR_InsertBillingHeader(
                                model.Tanggal,
                                model.Tanggal,
                                model.PaketObat,
                                model.Paket,
                                model.BiayaRacik,
                                model.SubTotal,
                                model.Dokter,
                                section,
                                model.SectionAsal,
                                model.NoResep,
                                model.NoReg,
                                model.KelebihanPlafon,
                                model.Keterangan,
                                model.IncludeJasa,
                                userid,
                                model.COB
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("FAR_InsertBillingHeader tidak mendapatkan nobukti");
                            var qtylock = 0;
                            foreach (var x in model.Detail)
                            {
                                //if(x.Aturan1 == null) throw new Exception("Aturan 1 Tidak Boleh Kosong");
                                //if(x.Aturan1 == null) throw new Exception("Aturan 2 Tidak Boleh Kosong");
                                if (x.JumlahObat == 0) throw new Exception("Jumlah Obat Tidak Boleh Kosong");
                                if (x.JumlahObat > x.QtyStok && x.Lock == true)
                                {
                                    qtylock += 1;
                                }
                                s.FAR_InsertBillingDetail(
                                    id,
                                    x.Id,
                                    x.JumlahObat,
                                    x.Aturan1,
                                    x.Aturan2,
                                    x.Harga,
                                    x.Disc,
                                    x.TanggalED,
                                    x.Racikan,
                                    x.WaktuTiket1,
                                    x.WaktuTiket2,
                                    x.WaktuTiket3,
                                    x.WaktuTiket4,
                                    x.WaktuTiket5,
                                    x.WaktuTiket6,
                                    x.WaktuTiket7,
                                    x.WaktuTiket8,
                                    x.WaktuTiket9,
                                    x.WaktuTiket10,
                                    x.WaktuTiket11,
                                    x.WaktuTiket12
                                );
                            }
                            s.HitungEstimasiBiaya(model.NoReg);
                            if (qtylock > 0)
                            {
                                throw new Exception("Qty Stok Tidak Mencukupi");
                            }
                            s.SaveChanges();
                        }

                        if (_process == "EDIT") 
                        {
                            s.FAR_Update_ListBillHeader(model.NoBukti, model.IncludeJasa, model.COB, model.Jam);
                            id = model.NoBukti;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiBilling-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_ListBillingResep_FN().Where(x => x.NoBukti == id).FirstOrDefault();
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.FAR_ListBillingResepDetail.Where(x => x.NoBukti == m.NoBukti).OrderBy(x => x.NamaBarang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd") == null ? "" : m.Tanggal.ToString("yyyy-MM-dd"),
                            Jam = $"{m.Jam:HH}:{m.Jam:mm}",
                            Dokter = m.DokterID,
                            DokterNama = m.NamaDOkter,
                            Spesialis = m.SpesialisName,
                            Phone_Reg = m.Phone_Reg,
                            SectionAsal = m.SectionAsalID,
                            KodePaket = m.KodePaket == null ? "" : m.KodePaket,
                            PaketObat = m.Paket ?? false,
                            Paket = m.PaketObat == null ? "" : m.PaketObat,
                            IncludeJasa = m.IncludeJasa == null ? false : m.IncludeJasa,
                            NoResep = m.NoResep == null ? "" : m.NoResep,
                            TanggalResep = m.TglResep == null ? "" : m.TglResep.Value.ToString("yyyy-MM-dd"),
                            JamResep = m.TglResep == null ? "" : $"{m.TglResep:HH}:{m.TglResep:mm}",
                            ////TanggalResep = m.TglResep == null ? "" : m.TglResep.Value.ToString("yyyy-MM-dd"),
                            Cito = m.Cito == null ? false : m.Cito,
                            NoReg = m.NoReg,
                            NRM = m.NRM,
                            Nama = m.NamaPasien,
                            Alamat = m.Alamat,
                            Gender = m.JenisKelamin,
                            EstimasiSisa = m.EstimasiSisa,
                            UmurTahun = m.UmurThn,
                            UmurBulan = m.UmurBln,
                            BeratBadan = m.BeratBadan,
                            Kerjasama = m.JenisKerjasama == null ? "" : m.JenisKerjasama,
                            NoKartu = m.NoKartu == null ? "" : m.NoKartu,
                            Keterangan = m.Keterangan,
                            BiayaRacik = m.BiayaRacik ?? 0,
                            KelebihanPlafon = m.TotalKelebihanPlafon ?? 0,
                            COB = m.COB == null ? false : m.COB,
                            SudahTelaahResep = s.Farmasi_trTelaahResep_Check(m.NoResep).FirstOrDefault(),
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Aturan1 = x.Aturan1 == null ? "" : x.Aturan1,
                            Aturan2 = x.Aturan2 == null ? "" : x.Aturan2,
                            TglED = x.TglED == null ? "" : x.TglED.Value.ToString("yyyy-MM-dd"),
                            JumlahResep = x.JmlResep,
                            JumlahObat = x.JmlObat,
                            Harga = x.Harga,
                            Stok = x.Stok,
                            Disc = x.Disc,
                            Racikan = x.NamaRacikan,
                            Eresep1 = x.WaktuTiket1 ?? false,
                            Eresep2 = x.WaktuTiket2 ?? false,
                            Eresep3 = x.WaktuTiket3 ?? false,
                            Eresep4 = x.WaktuTiket4 ?? false,
                            Eresep5 = x.WaktuTiket5 ?? false,
                            Eresep6 = x.WaktuTiket6 ?? false,
                            Eresep7 = x.WaktuTiket7 ?? false,
                            Eresep8 = x.WaktuTiket8 ?? false,
                            Eresep9 = x.WaktuTiket9 ?? false,
                            Eresep10 = x.WaktuTiket10 ?? false,
                            Eresep11 = x.WaktuTiket11 ?? false,
                            Eresep12 = x.WaktuTiket12 ?? false,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - P A K E T

        [HttpPost]
        public string ListLookupPaket(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_GetPaketHeader.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.KodePaket.Contains(x.Value) ||
                                y.NamaPaket.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.KodePaket,
                        Nama = m.NamaPaket
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailPaket(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.FAR_GetPaketDetail.Where(x => x.KodePaket == id).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Nama_Satuan,
                            JumlahResep = x.JmlObat,
                            JumlahObat = x.JmlResep,
                            Stok = x.QtyStok,
                            Harga = x.Harga,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B A R A N G

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var noreg = filter.FirstOrDefault(x => x.Key == "NoReg").Value;

                    var proses = s.FAR_GetListObat(section, noreg).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.NamaBarang.Contains(x.Value) ||
                                y.Nama_Kategori.Contains(x.Value));
                    }
                    var transaksi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (transaksi.ProsesPayment == true) throw new Exception("Data pasien sedang di proses dikasir, silahkan hubungi kasir !!");
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();

                    foreach (var m in models)
                    {
                        var h = s.FT_GetHarga_Barang_Pelayanan_Skalar(m.Barang_ID, section, "HargaJual", noreg).FirstOrDefault();
                        m.Harga_Jual = h ?? 0;
                    }


                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.NamaBarang,
                        Satuan = x.Satuan_Stok,
                        Stok = x.Qty_Stok,
                        Kategori = x.Nama_Kategori,
                        QtyStok = x.Qty_Stok,
                        LockStock = x.LockStock,
                        ProsesTransaksi = transaksi.ProsesPayment,
                        Aturan2 = x.AturanPakai_2,
                        Harga = x.Harga_Jual ?? 0
                    }); 
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E G I S T R A S I

        [HttpPost]
        public string ListLookupRegistrasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;
                    var proses = s.FAR_GetRegistrasi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoReg.ToString().Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.Alamat.Contains(x.Value) ||
                                y.NamaDokter.Contains(x.Value) ||
                                y.DokterID.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                //y.Kamar.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoReg = x.NoReg,
                        Tanggal = x.TglReg.ToString("dd/MM/yyyy"),
                        RawatInap = x.RawatInap,
                        SectionAsal = x.SectionAsalID,
                        DokterID = x.DokterID,
                        NamaDokter = x.NamaDokter,
                        Phone_Reg = x.Phone_Reg,
                        NRM = x.NRM,
                        Nama = x.NamaPasien,
                        Gender = x.JenisKelamin,
                        Perusahaan = x.Nama_Customer,
                        NoKartu = x.NoKartu,
                        Alamat = x.Alamat,
                        UmurTahun = x.UmurThn,
                        UmurBulan = x.UmurBln,
                        BeratBadan = x.BB,
                        EstimasiSisa = x.EstimasiSisa

                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E S E P

        [HttpPost]
        public string ListLookupResep(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_GetResepHeader.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoResep.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.NamaDOkter.Contains(x.Value) ||
                                y.NoRegistrasi.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoResep = m.NoResep,
                        Tanggal = m.TglResep.ToString("yyyy-MM-dd") == null ? "" : m.TglResep.ToString("yyyy-MM-dd"),
                        InputTanggal = m.TglResep.ToString("yyyy-MM-dd") == null ? "" : m.TglResep.ToString("yyyy-MM-dd"),
                        InputJam = $"{m.JamResep.ToString("HH")}:{m.JamResep.ToString("mm")}",
                        Cito = m.Cyto,
                        Dokter = m.DokterID,
                        Registrasi = m.NoRegistrasi,
                        Section = m.SectionAsal,
                        Nama = m.NamaPasien,
                        Gender = m.Gender,
                        NRM = m.NRM,
                        JenisPasien = m.JenisPasien,
                        Perusahaan = m.Perusahaan,
                        SectionAsalId = m.SectionAsalID,

                        PaketObat = m.Paket ?? false,
                        KodePaket = m.KodePaket,
                        Paket = m.NamaPaket,

                        NoReg = m.NoRegistrasi,
                        Alamat = m.Alamat,
                        UmurTahun = m.UmurThn,
                        UmurBulan = m.UmurBln,
                        BeratBadan = m.BeratBadan,
                        Kerjasama = m.Perusahaan,
                        NoKartu = m.NoKartu
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailResep(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.FAR_GetResepDetail.Where(x => x.NoResep == id).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Aturan1 = x.AturanPakai,
                            Aturan2 = "",
                            TglED = "",
                            JumlahResep = x.JmlResep,
                            JumlahObat = x.JmlResep,
                            Stok = x.Stok,
                            Harga = x.Harga,
                            Racikan = x.Racik
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P  H I S T O R Y  O B A T  P A S I E N

        [HttpGet]
        public string Detailhistory(string nrm)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt = s.Farmasi_HistoryPemakaianObatPasien_byNRM(nrm).ToList();

                    if (dt != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Tidak ada history"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion

        #region ===== L O O K U P  H I S T O R Y  K U N J U N G A N

        [HttpGet]
        public string DetailhistoryKunjungan(string nrm)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt_kunjungan = s.Vw_StatusKunjunganPasien.Where(x => x.NRM == nrm).ToList();

                    if (dt_kunjungan != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt_kunjungan,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Tidak ada history"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion

        #region ===== S E T U P - T E L A A H  R E S E P

        [HttpGet]
        public string checkTelaahFarmasi(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            if (nobukti == "") {
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = false,
                    Jenis = "Validasi",
                    Message = "No resep masih kosong."
                });
            }

            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var pesan = s.Farmasi_trTelaahResep_Check(nobukti).FirstOrDefault();
                    if (pesan == "Sudah Ada")
                    {
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahResep.ToList();
                        foreach (var x in m)
                        {
                            var dt_telaah = s.Farmasi_trTelaahResepDetail.Where(xx => xx.NoBukti == nobukti && xx.NoUrut == x.NoUrut).FirstOrDefault();
                            if (dt_telaah == null)
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = false,
                                    Keterangan = ""
                                });
                            }
                            else
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = dt_telaah.Iya,
                                    Keterangan = dt_telaah.Keterangan
                                });
                            }
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                    else
                    {
                        //return JsonConvert.SerializeObject(new
                        //{
                        //    IsSuccess = true,
                        //    Data = pesan,
                        //    Message = "-"
                        //});
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahResep.ToList();
                        foreach (var x in m)
                        {
                            dt_new.Add(new
                            {
                                NoUrut = x.NoUrut,
                                Telaah = x.Telaah,
                                Iya = true,
                                Keterangan = "-"
                            });
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpGet]
        public string dataTelaahFarmasi(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt = s.Farmasi_mTelaahResep.ToList();
                    var gettelaahfarmasi = s.Farmasi_trTelaahResepDetail.Where(x => x.NoBukti == nobukti).ToList();
                    if (dt != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {

                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string postTelaahFarmasi(string nobukti, List<detailTelaahFarmasi> option)
        {

            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {

                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var userid = User.Identity.GetUserId();
                    var dt_telaahcek = s.Farmasi_trTelaahResep.Where(x => x.NoBukti == nobukti).FirstOrDefault();
                    if (dt_telaahcek == null)
                    {
                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {

                            var dt = s.Farmasi_trTelaahResep_insert(nobukti, userid);
                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahResep.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahResepDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }
                    else
                    {

                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0 || error > 0)
                        {
                            var Farmasi_trTelaahResepDetail = s.Farmasi_trTelaahResepDetail.Where(xx => xx.NoBukti == nobukti).ToList();
                            foreach (var xxx in Farmasi_trTelaahResepDetail)
                            {
                                s.Farmasi_trTelaahResepDetail.Remove(xxx);
                                s.SaveChanges();
                            }

                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahResep.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahResepDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                    }

                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion 

        #region ===== S E T U P - T E L A A H  O B A T  

        [HttpGet]
        public string checkTelaahObat(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var pesan = s.Farmasi_trTelaahObat_Check(nobukti).FirstOrDefault();
                    if (pesan == "Sudah Ada")
                    {
                        var dt_new = new List<object>();
                        var m = s.Farmasi_mTelaahObat.ToList();
                        foreach (var x in m)
                        {
                            var dt_telaah = s.Farmasi_trTelaahObatDetail.Where(xx => xx.NoBukti == nobukti && xx.NoUrut == x.NoUrut).FirstOrDefault();
                            if (dt_telaah == null)
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = false,
                                    Keterangan = ""
                                });
                            }
                            else
                            {
                                dt_new.Add(new
                                {
                                    NoUrut = x.NoUrut,
                                    Telaah = x.Telaah,
                                    Iya = dt_telaah.Iya,
                                    Keterangan = dt_telaah.Keterangan
                                });
                            }
                        }
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = dt_new,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = pesan,
                            Message = "-"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpGet]
        public string dataTelaahObat(string nobukti)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var dt = s.Farmasi_mTelaahObat.ToList();
                    var gettelaahfarmasi = s.Farmasi_trTelaahObatDetail.Where(x => x.NoBukti == nobukti).ToList();
                    if (dt != null)
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt,
                            Message = "-"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = false,
                            Data = "",
                            Message = "Data tidak ditemukan"
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string postTelaahObat(string nobukti, List<detailTelaahFarmasi> option)
        {

            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {

                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var userid = User.Identity.GetUserId();
                    var dt_telaahcek = s.Farmasi_trTelaahObat.Where(x => x.NoBukti == nobukti).FirstOrDefault();
                    if (dt_telaahcek == null)
                    {
                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0 || error > 0)
                        {

                            var dt = s.Farmasi_trTelaahObat_insert(nobukti, userid);
                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahObat.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahObatDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }
                    else
                    {

                        int error = 0;
                        foreach (var x in option)
                        {
                            if (x.cekTelaah == true)
                            {
                                if (x.ketTelaah == null)
                                {
                                    error += 1;
                                }
                            }
                        }

                        if (error == 0)
                        {
                            var Farmasi_trTelaahObatDetail = s.Farmasi_trTelaahObatDetail.Where(xx => xx.NoBukti == nobukti).ToList();
                            foreach (var xxx in Farmasi_trTelaahObatDetail)
                            {
                                s.Farmasi_trTelaahObatDetail.Remove(xxx);
                                s.SaveChanges();
                            }

                            foreach (var x in option)
                            {
                                var mTelaah = s.Farmasi_mTelaahObat.Where(sx => sx.NoUrut == x.NoUrut).FirstOrDefault();
                                if (x.cekTelaah == true)
                                {
                                    s.Farmasi_trTelaahObatDetail_insert(
                                        nobukti,
                                        x.NoUrut,
                                        mTelaah.Telaah,
                                        x.cekTelaah,
                                        x.ketTelaah
                                     );
                                }
                            }
                        }

                        if (error > 0)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = "",
                                Message = "Masih ada keterangan yang kosong"
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = "",
                                Message = "Data berhasil disimpan"
                            });
                        }
                    }

                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion 

        #region ===== L O O K U P - R E G I S T R A S I H I S T O R Y

        [HttpPost]
        public string ListLookupRegistrasiHistory(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;
                    var proses = s.Vw_Pasien.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NRM.Contains(x.Value) ||
                                y.Alamat.Contains(x.Value) ||
                                y.NoIdentitas.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                y.Phone.Contains(x.Value) ||
                                //y.Kamar.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        NoIdentitas = x.NoIdentitas == null ? "-" : x.NoIdentitas,
                        Tanggal = x.TglLahir.Value.ToString("dd/MM/yyyy"),
                        Alamat = x.Alamat,
                        Phone = x.Phone,

                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== H I S T O R Y   P E M A K A I A N   O B A T

        [HttpPost]
        public string ListHistoryPemakaianObat(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var nrm = filter.FirstOrDefault(x => x.Key == "All").Value;
                    var proses = s.Farmasi_HistoryPemakaianObatPasien_byNRM(nrm).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value)) { 
                            proses = proses.Where(y =>
                                y.NRM.Contains(x.Value));
                        }

                        if (x.Key == "PeriodeStartHistory" && !string.IsNullOrEmpty(x.Value))
                        {

                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.TglFarmasi >= d);
                        }
                        if (x.Key == "PeriodeEndHistory" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(+1);
                            proses = proses.Where(y => y.TglFarmasi <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();

                    var r = models.ConvertAll(x => new
                    {
                        Tanggal = x.Tanggal,
                        TglFarmasi = x.TglFarmasi.ToString("yyyy-MM-dd"),
                        Nama_Barang = x.Nama_Barang,
                        AturanPakai = x.AturanPakai,
                        JmlObat = x.JmlObat,
                        HargaSatuan = x.HargaSatuan,
                        AsalPasien = x.AsalPasien,
                        NoBukti = x.NoBukti
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== TASK_06
        [HttpPost]
        public string SaveTask06(string noreg)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var tanggalsekarang = DateTime.Now;
                    s.BPJS_Task_Insert(noreg, "TaskNo_6", tanggalsekarang, tanggalsekarang, tanggalsekarang);
                    s.SaveChanges();
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Task_06;".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                    //dbContextTransaction.Commit();

                    return JsonConvert.SerializeObject(new { IsSuccess = true });
                }
                catch (SqlException ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                //}
            }
        }
        #endregion

        #region ===== H A P U S  B I A Y A  R A C I K 
        [HttpPost]
        public string HapusBiayaRacik(string nobukti)
        {
            using (var s = new SIMEntities())
            {
                //using (var dbContextTransaction = s.Database.BeginTransaction())
                //{
                try
                {

                    s.FAR_HapusBiayaRacik(nobukti);

                    s.SaveChanges();
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Hapus Biaya Racik-PROSES;".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                    //dbContextTransaction.Commit();

                    return JsonConvert.SerializeObject(new { IsSuccess = true });
                }
                catch (SqlException ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                //}
            }
        }
        #endregion
    }
}