﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiBillingObatBebasController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListBillingObatBebas.Where(x => x.Farmasi_SectionID == section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Registrasi_NoReg.Contains(x.Value) ||
                                y.Registrasi_NoKartu.Contains(x.Value) ||
                                y.Resep_NoResep.Contains(x.Value) ||
                                y.NoKartu.Contains(x.Value) ||
                                y.NoBukti.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.NamaDokter.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(-1);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value).AddDays(+2);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{m.Jam.ToString("HH")}:{m.Jam.ToString("mm")}",
                        NoReg = m.Registrasi_NoReg,
                        NRM = m.NRM,
                        Nama = m.NamaPasien,
                        Gender = m.Registrasi_JenisKelamin,
                        Kerjasama = m.Kerjasama_JenisKerjasama,
                        Perusahaan = m.Registrasi_Nama_Customer,
                        Dokter = m.NamaDokter,
                        NoKartu = m.Registrasi_NoKartu,
                        Spesialis = m.Dokter_SpesialisName,
                        Section = m.SectionAsal_SectionName,
                        NoResep = m.Resep_NoResep,
                        Batal = m.Batal,
                        ClosePayment = m.ClosePayment ?? false,
                        Audit = m.IncomeAudit,
                        User = ""
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiBillingObatBebasViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                            var section = Request.Cookies["Inventory_Section_Id"].Value;

                            if (model.Detail == null) model.Detail = new List<FarmasiBillingObatBebasDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            if (model.CaraBayar == "BonJKN" && model.NoReg == null) throw new Exception("No Reg Tidak Boleh Kosong");
                            var grandtotal = model.SubTotal + model.BiayaRacik;
                            id = s.FAR_InsertBillingObatBebas(
                                model.Tanggal,
                                model.Tanggal,
                                model.BiayaRacik,
                                model.SubTotal,
                                model.Dokter,
                                section,
                                model.Nama,
                                model.TipePasien,
                                model.Perusahaan,
                                model.NoKartu,
                                model.Karyawan,
                                userid,
                                model.CaraBayar == "Tunai" ? grandtotal : 0,
                                model.CaraBayar == "Bon" ? grandtotal : 0,
                                model.NoReg,
                                model.CaraBayar == "BonJKN" ? grandtotal : 0
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("FAR_InsertBillingObatBebas tidak mendapatkan nobukti");
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            var qtylock = 0;
                            foreach (var x in model.Detail)
                            {
                                //if (x.TanggalED == null) throw new Exception("Tgl tidak boleh kosong");
                                if (x.Disc > 100) throw new Exception("Diskon Tidak Boleh melebihi 100%");
                                if (x.JumlahObat == 0) throw new Exception("Jumlah Obat Tidak Boleh Kosong");
                                if (x.JumlahObat > x.QtyStok && x.Lock == true)
                                {
                                    qtylock += 1;
                                }
                                s.FAR_InsertBillingObatBebasDetail(
                                        id,
                                        x.Id,
                                        x.JumlahObat,
                                        x.Aturan1,
                                        x.Aturan2,
                                        x.TanggalED,
                                        x.Harga,
                                        x.Disc,
                                        x.Racik,
                                        x.WaktuTiket1,
                                        x.WaktuTiket2,
                                        x.WaktuTiket3,
                                        x.WaktuTiket4,
                                        x.WaktuTiket5,
                                        x.WaktuTiket6,
                                        x.WaktuTiket7,
                                        x.WaktuTiket8,
                                        x.WaktuTiket9,
                                        x.WaktuTiket10,
                                        x.WaktuTiket11,
                                        x.WaktuTiket12
                                    );

                            }
                            if (qtylock > 0)
                            {
                                throw new Exception("Qty Stok Tidak Mencukupi");
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            //s.FAR_ReturObatBebas(model.NoBukti);
                            var cekretur = s.FAR_ReturObatBebas(model.NoBukti).FirstOrDefault();
                            if (cekretur == "Proses Retur Tidak Bisa Dilakukan, Karena Pasien Telah Melakukan Pembayaran Dikasir") throw new Exception(cekretur);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiBillingObatBebas-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }

            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FAR_ListBillingObatBebasHeader.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.FAR_ListBillingObatBebasDetail.Where(x => x.NoBukti == m.NoBukti).OrderBy(x => x.Nama_Barang).ToList();
                    //return JsonConvert.SerializeObject(new {
                    //    IsSuccess = true,
                    //    Data = new 
                    //    {
                    //        NoBukti = id
                    //    },
                    //    Detail = new List<object>()
                    //});
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Dokter = m.DokterID,
                            Spesialis = m.SpesialisName,
                            TipePasien = m.TipePasienID,
                            Tanggal = m.Tanggal,
                            KodePerusahaan = m.KodePerusahaan,
                            Perusahaan = m.JenisKerjasama,
                            NoKartu = m.NoKartu,
                            NoReg = m.NoReg,
                            Nama = m.NamaPasien,
                            Karyawan = m.Karyawan,
                            SubTotal = m.Total ?? 0,
                            BiayaRacik = m.BiayaRacik ?? 0,
                            TipeBayar = m.TipeBayar
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.NamaBarang,
                            Satuan = x.Satuan,
                            Aturan1 = x.Aturan1,
                            Aturan2 = x.Aturan2,
                            TglED = x.TglED == null ? "" : x.TglED.Value.ToString("yyyy-MM-dd"),
                            JumlahObat = x.JmlObat,
                            Stok = x.Stok,
                            Harga = x.Harga,
                            Disc = x.Disc,
                            Racikan = x.NamaRacikan,
                            Eresep1 = x.WaktuTiket1 ?? false,
                            Eresep2 = x.WaktuTiket2 ?? false,
                            Eresep3 = x.WaktuTiket3 ?? false,
                            Eresep4 = x.WaktuTiket4 ?? false,
                            Eresep5 = x.WaktuTiket5 ?? false,
                            Eresep6 = x.WaktuTiket6 ?? false,
                            Eresep7 = x.WaktuTiket7 ?? false,
                            Eresep8 = x.WaktuTiket8 ?? false,
                            Eresep9 = x.WaktuTiket9 ?? false,
                            Eresep10 = x.WaktuTiket10 ?? false,
                            Eresep11 = x.WaktuTiket11 ?? false,
                            Eresep12 = x.WaktuTiket12 ?? false,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - P E R U S A H A A N

        [HttpPost]
        public string ListLookupPeusahaan(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_ListCustomerKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Customer.Contains(x.Value) ||
                                y.NamaKelas.Contains(x.Value) ||
                                y.JenisKerjasama.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.Kode_Customer,
                        Nama = m.Nama_Customer,
                        Kelas = m.NamaKelas,
                        Jenis = m.JenisKerjasama,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - B A R A N G

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;

                    var noreg = "";

                    var proses = s.FAR_GetListObat(section, noreg).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.NamaBarang.Contains(x.Value) ||
                                y.Nama_Kategori.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();

                    foreach (var m in models)
                    {
                        var h = s.FT_GetHarga_Barang_Pelayanan_Skalar(m.Barang_ID, section, "HargaJual", "").FirstOrDefault();
                        m.Harga_Jual = h ?? 0;
                    }

                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.NamaBarang,
                        Satuan = x.Satuan_Stok,
                        Stok = x.Qty_Stok,
                        Kategori = x.Nama_Kategori,
                        QtyStok = x.Qty_Stok,
                        LockStock = x.LockStock,
                        Aturan2 = x.AturanPakai_2,
                        Harga = x.Harga_Jual ?? 0
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E G I S T R A S I

        [HttpPost]
        public string ListLookupRegistrasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                    var section = Request.Cookies["Inventory_Section_Id"].Value;
                    var proses = s.FAR_GetRegistrasi_ObatBebas.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoReg.ToString().Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.Alamat.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                //y.Kamar.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoReg = x.NoReg,
                        Tanggal = x.TglReg.ToString("dd/MM/yyyy"),
                        RawatInap = x.RawatInap,
                        SectionAsal = x.SectionAsalID,
                        Phone_Reg = x.Phone_Reg,
                        NRM = x.NRM,
                        Nama = x.NamaPasien,
                        Gender = x.JenisKelamin,
                        Perusahaan = x.Nama_Customer,
                        NoKartu = x.NoKartu,
                        Alamat = x.Alamat,
                        UmurTahun = x.UmurThn,
                        UmurBulan = x.UmurBln,
                        BeratBadan = x.BB,
                        EstimasiSisa = x.EstimasiSisa

                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion


        #region ===== H A P U S  B I A Y A  R A C I K 
        [HttpPost]
        public string HapusBiayaRacik(string nobukti)
        {
            using (var s = new SIMEntities())
            {
                //using (var dbContextTransaction = s.Database.BeginTransaction())
                //{
                try
                {

                    s.FAR_HapusBiayaRacik(nobukti);

                    s.SaveChanges();
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Hapus Biaya Racik-PROSES;".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                    //dbContextTransaction.Commit();

                    return JsonConvert.SerializeObject(new { IsSuccess = true });
                }
                catch (SqlException ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                //}
            }
        }
        #endregion

    }
}