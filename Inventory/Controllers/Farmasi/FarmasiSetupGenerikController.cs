﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    [Authorize(Roles = "Inventory")]
    public class FarmasiSetupGenerikController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.FAR_mGenerik.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Generik_Name.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Generik_ID,
                        Nama = x.Generik_Name
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, FarmasiSetupGenerikModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.FAR_InsertGenerik(model.Nama);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.FAR_UpdateGenerik(model.Nama, model.Id);
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"FarmasiSetupGenerik-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.FAR_mGenerik.FirstOrDefault(x => x.Generik_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Generik_ID,
                            Nama = m.Generik_Name
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}