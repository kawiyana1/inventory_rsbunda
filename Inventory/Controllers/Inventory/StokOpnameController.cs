﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class StokOpnameController : Controller
    {
        private string tipepelayanan = "GUDANG";
        private string tipepelayanan2 = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            var model = new StokOpnameViewModel();
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            using (var s = new SIMEntities())
            {
                model.Setup = "1";
                var setup = s.SetupAwal.FirstOrDefault(x => x.SetupName == "QtyFisikOpnameNol");
                if (setup != null)
                {
                    model.Setup = setup.Nilai.ToString();
                }
            }
            ViewBag.Setup = model.Setup;
            return View(model);
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListStokOpname.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => 
                            y.No_Bukti.Contains(x.Value) ||
                            y.KelompokJenis.Contains(x.Value) ||
                            y.Nama_Kategori.Contains(x.Value) ||
                            y.Nama_Sub_Kategori.Contains(x.Value) ||
                            y.Keterangan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Opname >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Opname <= d);
                        }
                    }
                    //var tahun = int.Parse(filter.FirstOrDefault(x => x.Key == "Tahun").Value);
                    //var bulan = int.Parse(filter.FirstOrDefault(x => x.Key == "Bulan").Value);
                    //var _start = new DateTime(tahun, bulan, 1);
                    //var _end = new DateTime(tahun, bulan, DateTime.DaysInMonth(tahun, bulan));
                    //proses = proses.Where(y => y.Tgl_Opname >= _start && y.Tgl_Opname <= _end);

                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        No = m.No_Bukti,
                        Posted = m.Posted,
                        Tanggal = m.Tgl_Opname.ToString("dd/MM/yyyy"),
                        Kelompok = m.KelompokJenis,
                        SubKategoriID = m.Nama_Sub_Kategori,
                        SubKategori = m.SubKategori_ID,
                        Keterangan = m.Keterangan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, StokOpnameViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<StokOpnameDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertStokOpname(
                                model.Tanggal,
                                model.KelompokJenis,
                                model.Lokasi,
                                userid,
                                0,
                                model.Inisial,
                                model.KodeKelompok,
                                model.KeteranganSO
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertStokOpname tidak mendapatkan nobukti");

                            foreach (var x in model.Detail)
                            {
                                s.INV_InsertStokOpnameDetail(
                                    id,
                                    x.Id,
                                    model.Lokasi,
                                    x.Qty,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            foreach (var x in model.Detail)
                            {
                                s.INV_UpdateStokOpnameDetail(
                                    model.NoBukti,
                                    x.Id,
                                    x.Qty,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"StokOpname-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListStokOpnameHeader.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListStokOpnameDetail.Where(x => x.No_Bukti == m.No_Bukti).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No = m.No_Bukti,
                            Tanggal = m.Tgl_Opname.ToString("yyyy-MM-dd"),
                            Lokasi = m.Lokasi_ID,
                            KodeKelompok = m.KodeKelompok,
                            //SubKategoriNama = m.Nama_Sub_Kategori,
                            Posted = m.Posted,
                            KelompokJenis = m.KelompokJenis,
                            Inisial = m.Keterangan,
                            KeteranganSO = m.KeteranganOpname
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.NamaSatuan,
                            Konversi = x.Konversi,
                            QtySystem = x.Stock_Akhir,
                            QtyFisik = x.Qty_Opname,
                            Harga = x.Harga_Rata,
                            Kategori = x.Nama_Kategori,
                            Keterangan = x.Keterangan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string ProsesSO(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        s.INV_ProsesSO(id);
                        s.SaveChanges();

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"StokOpname-ProsesSO; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                if (filter.FirstOrDefault(x => x.Key == "Subkategori").Value == null) throw new Exception("Sub Kategori Kosong");
                if (filter.FirstOrDefault(x => x.Key == "KelompokJenis").Value == null) throw new Exception("Kelompok Jenis Kosong");

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var kelompokjenis = filter.FirstOrDefault(x => x.Key == "KelompokJenis").Value;
                    int subkategori = int.Parse(filter.FirstOrDefault(x => x.Key == "Subkategori").Value);
                    var _lokasi = int.Parse(filter.FirstOrDefault(x => x.Key == "Lokasi").Value);
                    var proses = s.INV_ListLookupBarangOpname(_lokasi, 0, kelompokjenis).OrderBy(x => x.Nama_Barang).AsQueryable();
                    if (proses == null) throw new Exception("Sub Kategori Sudah Di SO");
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Nama_Kategori.Contains(x.Value) ||
                                y.Satuan.Contains(x.Value));
                        }
                        if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                 y.SubKategori_Id == subkategori
                                 );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang,
                        Jenis = m.KelompokJenis,
                        Satuan = m.Satuan,
                        Konversi = m.Konversi,
                        QtySystem = m.Qty_stok,
                        Kategori = m.SubKategori_Id,
                        KategoriNama = m.Nama_Sub_Kategori,
                        Harga = m.Harga,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }


        public string DetailBarangSO(string id, string kelompokjenis, string inisial)
        {
            //if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    if (kelompokjenis == "") throw new Exception("Sub Kategori Kosong");
                    int lokasiid = int.Parse(@Request.Cookies["Inventory_Section_LokasiId"].Value);
                    var d = s.INV_ListLookupBarangOpname(lokasiid, 0, kelompokjenis).OrderBy(x => x.Nama_Barang).ToList();
                    if (!string.IsNullOrEmpty(inisial))
                    {
                        d = d.Where(x => x.Nama_Barang.ToUpper().StartsWith(inisial.ToUpper())).ToList();
                    }
                    if (d.Count() == 0) throw new Exception("Data Tidak Ditemukan atau Sudah Di SO");

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Jenis = d[0].KelompokJenis,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Konversi = x.Konversi,
                            QtySystem = x.Qty_stok,
                            Kategori = x.Nama_Kategori,
                            Harga = x.Harga,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}