﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Inventory.Models.Farmasi
{
    public class FarmasiBillingViewModel
    {
        public DateTime Tanggal { get; set; }
        public TimeSpan Jam { get; set; }
        public string NoBukti { get; set; }
        public bool PaketObat { get; set; }
        public string Paket { get; set; }
        public decimal BiayaRacik { get; set; }
        public decimal SubTotal { get; set; }
        public string Dokter { get; set; }
        public string SectionAsal { get; set; }
        public string NoResep { get; set; }
        public string NoReg { get; set; }
        public decimal KelebihanPlafon { get; set; }
        public string Keterangan { get; set; }
        public bool IncludeJasa { get; set; }
        public bool COB { get; set; }
        public List<FarmasiBillingDetailViewModel> Detail { get; set; }
    }

    public class FarmasiBillingTelaahViewModel
    {
        public string NoBukti { get; set; }
        public List<FarmasiBillingTelaahDetailViewModel> DetailTelaah { get; set; }
 
    }

    public class FarmasiBillingTelaahDetailViewModel
    {
        public int Id { get; set; }
        public int NoUrut { get; set; }
        public string Telaah { get; set; }
        public bool Iya { get; set; }
        public string Keterangan { get; set; }
    }

    public class FarmasiBillingDetailViewModel {
        public int Id { get; set; }
        public double JumlahObat { get; set; }
        public double QtyStok { get; set; }
        public bool Lock { get; set; }
        public string Aturan1 { get; set; }
        public string Aturan2 { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
        public DateTime? TanggalED { get; set; } 
        public string Racikan { get; set; }
        public bool WaktuTiket1 { get; set; }
        public bool WaktuTiket2 { get; set; }
        public bool WaktuTiket3 { get; set; }
        public bool WaktuTiket4 { get; set; }
        public bool WaktuTiket5 { get; set; }
        public bool WaktuTiket6 { get; set; }
        public bool WaktuTiket7 { get; set; }
        public bool WaktuTiket8 { get; set; }
        public bool WaktuTiket9 { get; set; }
        public bool WaktuTiket10 { get; set; }
        public bool WaktuTiket11 { get; set; }
        public bool WaktuTiket12 { get; set; }
    }

    
}